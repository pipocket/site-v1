<?php 
	require 'm2brimagem.php';

	class upload
	{
		protected $files;
		protected $type;
		private $directory = UPLOAD_PATH;

		public function send($options)
		{
			$validate = $this->validate();

			if (isset($validate->status) && !$validate->status)
				return $validate;

			$dirs = explode('/', $options['dir']);
					
			if (!is_dir($this->directory . $options['dir'])) {
				$dir_current = null;

				foreach ($dirs as $dir) {
					if ($dir) {
						$dir_current .= DS . $dir;
						mkdir($this->directory . $dir_current, 0777);
					}
				}
			}

			$dir_current = null;

			foreach ($dirs as $dir) {
				if ($dir) {
					$dir_current .= DS . $dir;
					@chmod($this->directory . $dir_current, 0777);
				}
			}

			switch ($this->type) {
				case 'image':
					$PATH = $this->directory . $options['dir'] . DS . $this->files['file']['name'];

					if (move_uploaded_file($this->files['file']['tmp_name'], $PATH)) {
						// Resize
						if (isset($options['resize'])) {
							$objectImage = new m2brimagem;
							$objectImage->carrega($PATH);

							if (isset($options['resize'][0]) && isset($options['resize'][1]))
								$objectImage->redimensiona($options['resize'][0], $options['resize'][1]);
							elseif (isset($options['resize'][0]))
								$objectImage->redimensiona($options['resize'][0]);

							$objectImage->grava($PATH);
						}

						// Generates thumb
						if (isset($options['thumb'])) {
							if (!isset($objectImage))
								$objectImage = new m2brimagem;

							$objectImage->carrega($PATH);

							if (isset($options['thumb'][0]) && isset($options['thumb'][1]) && $options['thumb'][1] != 'auto')
								$objectImage->redimensiona($options['thumb'][0], $options['thumb'][1]);
							elseif (isset($options['thumb'][0]))
								$objectImage->redimensiona($options['thumb'][0]);

							$result = $objectImage->grava(str_replace($this->files['file']['name'], $this->files['file']['thumb_name'], $PATH), 100);

							// Grayscale thumb
							if (isset($options['thumb'][2]) && $options['thumb'][2]) {
								$graythumb = str_replace('_thumb', '_thumb_gray', $this->files['file']['thumb_name']);
								$objectImage->grava(str_replace($this->files['file']['name'], $graythumb, $PATH), 100, true);
							}
						}

						return (object) array('status' => true, 'name' => $this->files['file']['name'], 'thumb_name' => $this->files['file']['thumb_name'], 'type' => $this->type);
					}
					break;

				case 'file':
					$ext = end(explode('.', $this->files['file']['name']));
					$this->files['file']['name'] = isset($options['new_name']) ? $options['new_name'] . ".$ext" : $this->files['file']['name'];
					$PATH = $this->directory . $options['dir'] . DS . $this->files['file']['name'];

					if (move_uploaded_file($this->files['file']['tmp_name'], $PATH)) {
						return (object) array('status' => true, 'name' => $this->files['file']['name'], 'thumb_name' => $this->files['file']['thumb_name'], 'type' => $this->type);
					}
					break;
				
				default:
					return false;
					break;
			}
		}

		private function validate()
		{
			$this->type = $_POST['type'];

			if (!isset($_FILES) OR !$_FILES['file']['name']) {
				return (object) array('status' => false, 'message' => 'file-not-found');
				exit;
			}

			if (!isset($_POST['type']) OR !$_POST['type']) {
				return (object) array('status' => false, 'message' => 'type-not-found');
				exit;
			}

			foreach ($_FILES as $key => $file) {
				$FILE = explode('.', $file['name']);
				$EXT = count($FILE) > 1 ? end($FILE) : null;

				if (strlen($_FILES[$key]['name']) > 30) {
					$_FILES[$key]['name'] = substr($_FILES[$key]['name'], 0, 30);
				}

				$_FILES[$key]['name'] = str_replace(".$EXT", "", $file['name']);
				$_FILES[$key]['name'] = strtolower($_FILES[$key]['name']);
				$_FILES[$key]['name'] = str_replace(array(' ', '.', ',', '-'), array('_', '_', '_', '_'), $_FILES[$key]['name']);
				$_FILES[$key]['thumb_name'] = $_FILES[$key]['name'] . '_thumb';

				if ($this->type == 'image') {
					$exts = array('image/gif', 'image/jpg', 'image/jpeg', 'image/png');

					if (array_search($file['type'], $exts) === false) {
						return (object) array('status' => false, 'message' => 'not-image');
						exit;
					}
				}
				else {
					$exts = array('image/gif', 'image/jpg', 'image/jpeg', 'image/png');

					if (array_search($file['type'], $exts) !== false) {
						return (object) array('status' => false, 'message' => 'not-file');
						exit;
					}
				}

				$_FILES[$key]['thumb_name'] .= ".$EXT";
				$_FILES[$key]['name'] .= ".$EXT";
			}

			$this->files = $_FILES;
		}

		public function rename($file_name, $new_file_name, $dir)
		{
			$ext = explode('.', $file_name);
			$ext = end($ext);

			$file = "{$new_file_name}.{$ext}";

			@rename($this->directory . $dir . DS . $file_name, $this->directory . $dir . DS . $file);

			return true;
		}

		public function remove($file, $file_thumb, $dir)
		{
			@unlink($this->directory . $dir . DS . $file_thumb);
			return @unlink($this->directory . $dir . DS . $file);
		}
	}
?>