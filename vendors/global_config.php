<?php
	class global_config
	{
		private $action;
		private $enviroment;
		private $production_urls;
		public $is_production;

		function __construct()
		{
			$this->enviroment = $_SERVER['SERVER_NAME'];

			$this->production_urls = array(
					'pipocket.com' => true,
					'www.pipocket.com' => true
				);

			$this->is_production = array_key_exists( $this->enviroment, $this->production_urls );
		}

		public function set_database()
		{
			switch( $this->enviroment )
			{
				case 'local.pipocket.com':
					APP_DATABASE::$set = 'local';
					break;

				case 'dev.pipocket.com':
					APP_DATABASE::$set = 'development';
					break;

				case 'pipocket.com':
				case 'www.pipocket.com':
				case 'beta.pipocket.com':
					APP_DATABASE::$set = 'production';
					break;
			}
		}

		public function set_api_keys()
		{
			$api_keys = array();

			switch( $this->enviroment )
			{
				case 'local.pipocket.com':
				case 'dev.pipocket.com':
					$api_keys['FACEBOOK_APP_ID']			= '217873604970508';
					$api_keys['FACEBOOK_SECRET_ID'] 		= '657402c7a22dd5afd8cf163e94f1b0ff';

					$api_keys['TWITTER_CONSUMER_KEY']		= 'eRUtt8TyBvrOMbF2Q1ntqg';
					$api_keys['TWITTER_CONSUMER_SECRET'] 	= 'lOsQaqyodaogXnLJaOIgsuxQeONm1Ar6653UtdsU8s';
					$api_keys['TWITTER_OAUTH_CALLBACK'] 	= 'http://dev.pipocket.com/signup/twitter/callback/';
					$api_keys['TWITTER_OAUTH_CALLBACK_2'] 	= 'http://dev.pipocket.com/profile/twitter_callback/';

					$api_keys['FOURSQUARE_CLIENT_ID'] 		= '4Y4D4TDASLZLLQPQGOA5OEVTUKEBHQXZVRBMWOKGZ3BETF1F';
					$api_keys['FOURSQUARE_SECRET'] 			= 'UIXE1M4CZUCWLYSMJJPRUJRZDMT0KCLUYOUX3AJW14Z5CLKE';
					$api_keys['FOURSQUARE_PUSH_SECRET'] 	= 'VKDWTOULPZMMHZEVTKZWZMFXRRCRCD0WQYTDJWANWVD1X324';
					$api_keys['FOURSQUARE_URL_CALLBACK'] 	= 'http://dev.pipocket.com/profile/foursquare_authorize/';
					break;

				case 'pipocket.com':
				case 'www.pipocket.com':
				case 'beta.pipocket.com':
					$api_keys['FACEBOOK_APP_ID']			= '236883466401174';
					$api_keys['FACEBOOK_SECRET_ID'] 		= '395d68a0aec4787c8f0fcd60ac939e9f';

					$api_keys['TWITTER_CONSUMER_KEY']		= 'UOdpo7w2JxNH5iLHskpckQ';
					$api_keys['TWITTER_CONSUMER_SECRET'] 	= '3NsNBjtoYb4JLNhzKu9OhlOJuzcgQgrBdHaek3hTucU';
					$api_keys['TWITTER_OAUTH_CALLBACK'] 	= 'http://pipocket.com/signup/twitter/callback/';
					$api_keys['TWITTER_OAUTH_CALLBACK_2'] 	= 'http://pipocket.com/profile/twitter_callback/';

					$api_keys['FOURSQUARE_CLIENT_ID'] 		= 'UCQDAKFKYW2PEG50BHGQKQP5LH3GWQPOXQF4JJQFVAEKUEEB';
					$api_keys['FOURSQUARE_SECRET'] 			= 'EAMCKZ3GPTWUTDPO4IK45EBD4DVU0N33RE2QJHO2LLNAWOQ3';
					$api_keys['FOURSQUARE_PUSH_SECRET'] 	= 'ANCAIXBTED41RV2ZECD2I4MJG1GSYVHYQIRSZ5ICMFQBOMWI';
					$api_keys['FOURSQUARE_URL_CALLBACK'] 	= 'http://pipocket.com/profile/foursquare_authorize/';
					break;
			}

			return $api_keys;
		}

		public function set_upload( $media_only = false )
		{
			$upload = array();

			switch ( $this->enviroment ) {
				case 'local.pipocket.com':
					if ( !$media_only )
					$upload['UPLOAD_PATH'] 		= dirname( AURA_ROOT ) . DS . 'Media-files';

					$upload['MEDIA_FILES_URL']  = 'http://media-files.local.pipocket.com';
					break;

				case 'dev.pipocket.com':
					if ( !$media_only )
					$upload['UPLOAD_PATH'] 		= dirname( AURA_ROOT ) . DS . 'Media-files';

					$upload['MEDIA_FILES_URL']  = 'http://media-files.dev.pipocket.com';
					break;

				default:
					if ( !$media_only )
					$upload['UPLOAD_PATH'] 		= dirname( AURA_ROOT ) . DS . 'media-files.pipocket.com';

					$upload['MEDIA_FILES_URL']  = 'http://media-files.pipocket.com';
					break;
			}

			return $upload;
		}
	}
?>