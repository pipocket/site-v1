/**	
 * 
 * Aura PHP Framework
 * PHP version 5
 * 
 * @author			Paulo Martins <phmartins6@gmail.com>
 * @copyright		Copyright 2010/2011
 * @version			1.4.0
 * 
 * Framework js helper
 */

try {
	jQuery(function(){
		aura.init();
	});
}
catch(e) {
	alert('Aura PHP: jQuery not found.');
}

var aura = {
	
	path: {
		root: '',
		approot: '',
		http: ''
	},
	
	web: {
		root: '',
		css: '',
		data: '',
		img: '',
		js: ''
	},
	
	init: function() {
		this.filter();
	},
	
	filter: function() {
		$("form.framework-filter").submit(function(){
			var action = $(this).attr('action');
			var format_url_framework = '';
			var inputs = $(this).find('input[type=text]');
			var hidden_inputs = $(this).find('input[type=hidden][data-framework-get=true]');
			var checkboxs = $(this).find('input[type=checkbox]');
			var radios = $(this).find('input[type=radio]');
			var selects = $(this).find('select');
			
			inputs.each(function(){
				if ($(this).val())
					format_url_framework += $(this).attr('name') + '/' + $(this).val() + '/';
			});

			hidden_inputs.each(function(){
				if ($(this).val())
					format_url_framework += $(this).attr('name') + '/' + $(this).val() + '/';
			});
			
			selects.each(function(){
				if ($(this).val())
					format_url_framework += $(this).attr('name') + '/' + $(this).val() + '/';
			});
			
			if (format_url_framework)
				location.href = action + '/' + format_url_framework;
			else
				location.href = action;
			
			return false;
		});
	}
	
}