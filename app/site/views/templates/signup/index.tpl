{block name="facebook-meta"}
   <meta property="og:title" content="{$smarty.const.PROJECT_NAME}"/>
   <meta property="og:type" content="website"/>
   <meta property="og:image" content="{$html->img}/pipocket-90x90.jpg"/>
   <meta property="og:description" content="Que tal compartilhar suas impressões sobre o filme que acabou de ver? Conte-nos sua experiência, compartilhe com seus amigos, e redefina a crítica de cinema."/>
{/block}

{block name="body"}
<div class="container">
	<div class="row mobile">
		<div class="span4">
			<img src="{$html->img}/bg/cellphones.png" alt="">
			<img src="{$html->img}/bg/iphone.png" alt="">
		</div>
		<div class="span12 signup-text">
			<h2>Pipocket no celular</h2>
			<h3>Android e Iphone</h3>
			<p></p>
			<p>Melhore a experiência de uso. <br>
			Imagine sair do cinema e poder compartilhar imediatamente suas impressões sobre filme?</p>
			<h3>Em breve!</h3>
		</div>
	</div>
	<div class="row">
		<div class="span8 signup">
			<h1>{$TITLE_SIGNUP}</h1>
			<br/>
			<p>{$TEXT_SIGNUP_PART1}</p>
			<p>{$TEXT_SIGNUP_PART2}</p>
		</div>

		<div class="span6 signup offset2">
			<div class="alert-message block-message error hide">
			  <p><strong>{$ERROR_MESSAGE_OPS}</strong></p>

			  <ul>
			  	<li id="name" class="hide">{$ERROR_MESSAGE_REGISTER_NAME}</li>
			  	
			  	<li id="email" class="hide">{$ERROR_MESSAGE_REGISTER_EMAIL}</li>
			  	
			  	<li id="password" class="hide">{$ERROR_MESSAGE_REGISTER_PASSWORD}</li>

			  	<li id="password-limit" class="hide">{$ERROR_MESSAGE_REGISTER_PASSWORD_LIMIT}</li>

			  	<li id="confirm-password" class="hide">{$ERROR_MESSAGE_REGISTER_CONFIRM_PASSWORD}</li>

				<li id="email-verify" class="hide"><strong>{$ERROR_MESSAGE_REGISTER_EMAIL_VERIFY}</strong></li>	
			  </ul>
			</div>

			<div class="alert-message block-message info hide">
				<p><img src="{$html->img}/ico/ajax-loader-blue.gif" alt=""> <strong></strong></p>
			</div>

			<div class="alert-message block-message success hide"></div>
			
			<form action="{$aura->root}/{$PATH_REGISTER}" method="post" name="register">
				<fieldset>
					<div class="clearfix">
						<div class="input">
							<input type="text" name="name" class="span6" placeholder="{$FORMFIELD_NAME}" value="{$value_name|default:""}"/>
						</div>
					</div>
					<div class="clearfix">
						<div class="input">
							<input type="text" name="email" class="span6" placeholder="{$FORMFIELD_EMAIL}" value="{$value_email|default:""}" />
						</div>
					</div>
					<div class="clearfix">
						<div class="input">
							<input type="password" name="password" class="span6 insertLabel" placeholder="{$FORMFIELD_PASSWORD}"/>
						</div>
					</div>
					<div class="clearfix">
						<div class="input">
							<input type="password" name="confirm-password" class="span6 insertLabel" placeholder="{$FORMFIELD_CONFIRM_PASSWORD}"/>
						</div>
					</div>
					<div>
						<input type="submit" class="btn primary large fr" value="{$BUTTON_SAVE_REGISTER}" onClick="Site.analytics.track_event(this, 'Button', 'Register'); return false;" />
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
{/block}

{block name="script"}
	Site.register({if isset($redefine_password) && $redefine_password}true{/if});
{/block}