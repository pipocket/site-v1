{block name="facebook-meta"}
   <meta property="og:title" content="{$smarty.const.PROJECT_NAME}"/>
   <meta property="og:type" content="website"/>
   <meta property="og:image" content="{$html->img}/pipocket-90x90.jpg"/>
   <meta property="og:description" content="Que tal compartilhar suas impressões sobre o filme que acabou de ver? Conte-nos sua experiência, compartilhe com seus amigos, e redefina a crítica de cinema."/>
{/block}

{block name="body-class"}nobar{/block}

{block name="body"}
<div class="container">
	<div class="span16 terms-privacy">
		<h1>{$LABEL_ABOUT}</h1>

		<p>{$TEXT_ABOUT_DESCRIPTION}</p>

		<br/><br/>

		<h3>{$TITLE_ABOUT_RULES}</h3>
		<p>{$TEXT_ABOUT_RULES}</p>
	</div>
</div>
{/block}