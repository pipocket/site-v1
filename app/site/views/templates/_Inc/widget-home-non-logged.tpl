<div class="row">
	<div class="span7 welcome">
		{if $logged}
		
		{else}
		<h1>{$TITLE_JOIN}</h1>
		<h3>Bem-vindo ao Pipocket</h3>
		<p>{$TEXT_JOIN}</p>

		<a href="{$aura->root}/{$PATH_JOIN}" class="btn primary large fr db" onClick="Site.analytics.track_event(this, 'Button', 'Join'); return false;">{$BUTTON_JOIN}</a>
		{/if}
	</div>

	<div class="span8 offset1 premieres non-logged">
		<ul class="unstyled">
			{foreach from=$releases item=release}
			<li>
				<a href="{$aura->root}/{$PATH_MOVIE}/{$release->key}" rel="twipsy" title="{$release->name}" data-offset="5">
					{if $release->thumb}
					<img src="{$smarty.const.MEDIA_FILES_URL}/movies/{$release->thumb}" alt="{$release->release}" width="123" height="174"/>
					{else}
					<img src="{$html->img}/no-image-poster.jpg" alt="{$release->release}">
					{/if}
				</a>
			</li>
			{/foreach}
		</ul>
	</div>
</div>