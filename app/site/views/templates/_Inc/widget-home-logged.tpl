<div class="row">
	<div class="span11">
		<div class="premieres user-logged">
			<ul class="unstyled">
				{foreach from=$releases item=release key=key}
				
				<li>
					<a href="{$aura->root}/{$PATH_MOVIE}/{$release->key}" rel="twipsy" title="{$release->name}">
						{if $release->thumb}
						<img src="{$smarty.const.MEDIA_FILES_URL}/movies/{$release->thumb}" alt="{$release->release}" />
						{else}
						<img src="{$html->img}/no-image-thumb2.jpg" alt="{$release->release}">
						{/if}
					</a>
				</li>
				
				{/foreach}
			</ul>
		</div>
	</div>
	<div class="span5">
		<ul class="unstyled fr home-comments-list">
			{foreach from=$comments item=comment}
			<li>
				<p>
					<a href="{$aura->root}/{$PATH_PROFILE}/{$comment->user_id}">{$comment->user}</a> sobre <a href="{$aura->root}/{$PATH_MOVIE}/{$comment->movie_key}" title="{$comment->title}">{$comment->movie}</a><br/> {$comment->description} <small class="db ar">{$LABEL_NOTE} {$comment->rate}</small>
				</p>
			</li>
			{/foreach}
		</ul>
	</div>
</div>