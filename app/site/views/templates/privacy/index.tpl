{block name="facebook-meta"}
   <meta property="og:title" content="{$smarty.const.PROJECT_NAME}"/>
   <meta property="og:type" content="website"/>
   <meta property="og:image" content="{$html->img}/pipocket-90x90.jpg"/>
   <meta property="og:description" content="Que tal compartilhar suas impressões sobre o filme que acabou de ver? Conte-nos sua experiência, compartilhe com seus amigos, e redefina a crítica de cinema."/>
{/block}

{block name="body-class"}nobar{/block}

{block name="body"}
<div class="container">
	<div class="span16 terms-privacy">
		<h1>{$LABEL_PRIVACY}</h1>

		<p>
			Informações e atividades do usuário são coletadas para medir o uso de nossos serviços e assim melhorá-los ao longo do tempo.
		</p>
		<br/><br/>

		<h3>DADOS PESSOAIS</h3>
		<p>
			Ao fazer parte do Pipocket algumas informações pessoais são fornecidas, algumas como Nome, link para Perfil do Facebook ou Twitter, são exibidas publicamente pelos nossos serviços, salvo no quando o usuário opta por não exibir tais informações.
		</p>
		<br/>

		<h3>DADOS ADICIONAIS</h3>
		<p>
			O usuário pode fornecer informações adicionais após o cadastro, como sexo, cidade e país onde vive. O sexo é usado apenas para fins de estatísticas, e o local onde vive é exibido em seu perfil somente quando solicitado.
		</p>
		<br/>

		<h3>SERVIÇOS DE TERCEIROS</h3>
		<p>
			O Pipocket é integrado a algumas redes sociais, tal postura serve para facilitar e/ou entreter os usuários do site.
		</p>
		<br/>

		<h3>Twitter</h3>
		<p>
			O usuário poderá usá-lo como meio de login, e compatilhar informações do Pipocket em seu perfil do Twitter.
		</p>
		<br/>

		<h3>Facebook</h3>
		<p>
			O usuário poderá usá-lo como meio de login, e compartilhar indormações do Pipocket em seu perfil do Facebook.
		</p>
		<br/>
		<!-- 
		<h3>Foursquare</h3>
		<p>
			O usuário poderá autorizar o Pipocket a coletar seus últimos checkins para fins de pontuação, ou seja, ao votar em um filme o usuário poderá informar o cinema onde assistiu, ganhando pontos de cinéfilo.
		</p>
 		-->
	</div>
</div>
{/block}