<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<title>Pipocket - Confirmation</title>
</head>
<body style="padding:0; margin:0;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center" valign="top">
				<table width="658" cellspacing="0" cellpadding="0">
					<tr>
						<td bgcolor="#2b2b2b" height="30">
							<table width="100%" cellspacing="0" cellpadding="10">
								<tr>
									<td><img src="http://#SITE_URL/media-files/mail/logo.png" alt="" /></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td align="center">
							<table width="95%" cellspacing="0" cellpadding="0" bgcolor="#f5f5f5">
								<tr>
									<td align="right"><img src="http://#SITE_URL/media-files/mail/top-corner.gif" alt="" /></td>
								</tr>
								<tr>
									<td>
										<table width="100%" cellspacing="0" cellpadding="10" bgcolor="#f5f5f5">
											<tr>
												<td style="font-family: Arial; font-size:13px; line-height:16px; color:#2b2b2b;">
													Ol&aacute; <b>#USER_NAME</b>. <br />
													Parece que voc&ecirc; esqueceu sua senha, por segurança iremos redefini-la, para continuar <a href="http://#SITE_URL/redefine-password/#USER_EMAIL" style="color:#888888;">clique aqui</a>. <br />
													Caso n&atilde;o tenha solicitado, desconsidere este e-mail.

													<br /><br /><br />

													Atencionsamente, <br />
													<b>Equipe Pipocket</b>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="right"><img src="http://#SITE_URL/media-files/mail/bottom-corner.gif" alt="" /></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td align="center" style="font-family: Arial; font-size:11px; line-height:12px; text-align:center; color:#2b2b2b;">
							Esta mensagem foi enviada automaticamente. Por favor não responda a este e-mail.
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>