<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
  <meta charset="utf-8">
  <title>{block name='title'}{/block}{$smarty.const.PROJECT_NAME}</title>

  <meta name="author" content="Pipocket">
  <meta name="description" content="Pipocket. Compartilhando Opiniões!">
  <meta name="country" content='Brazil' />
  <meta name="geo.country" content='BR' />
  <meta name="google-site-verification" content="7mQOnhI3Ip1rpEUH4WHQS1gMj5BXM6fVuTNMeFO6sR8" />

  <meta property="og:url" content="http://{$current_url}"/>
  {block name="facebook-meta"}{/block}
  
  <link rel="shortcut icon" href="{$aura->root}/favicon.ico">
  <link rel="stylesheet" type="text/css" href="{$html->css}/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="{$html->css}/site.css">
  <link rel="stylesheet" type="text/css" href="{$html->css}/caption.css">
  <script type="text/javascript" src="{$html->js}/libs/jquery-1.7.1.min.js"></script>
  <script type="text/javascript" src="{$html->js}/site.js"></script>
  <script type="text/javascript" src="{$html->js}/bootstrap-modal.js"></script>
  <script type="text/javascript" src="{$aura->js}"></script>
  {$aura->jsvars}
  {include file="_Inc/analytics.tpl"}
</head>

<body class="{block name='body-class'}{/block} {if $logged}user-logged{/if}">
  <div id="fb-root"></div>
  <div class="top">
    <h1><a href="{$aura->root}/" class="no-decoration" onClick="Site.analytics.track_event(this, 'Site', 'Logo'); return false;">{$smarty.const.PROJECT_NAME}</a></h1>
    {if isset($logged) AND $logged}
    <ul class="unstyled logged">
      <li>
        <form action="{$aura->root}/{$PATH_SEARCH}" name="search" method="get" class="framework-filter">
          <input type="submit" class="submit-search" value="ok" onClick="Site.analytics.track_event(this, 'Search', 'Topbar'); return false;">
          <input type="text" name="{$FORMNAME_TITLE}" class="span4" placeholder="{$PLACEHOLDER_FIND}" value="{$title|default:''}">
          <input type="hidden" name="{$FORMNAME_TYPE}" value="todos" />
        </form>
      </li>
      <li class="divider"></li>
      <li>
        <a href="{$aura->root}/{$PATH_SEARCH}/tipo/todos/" onClick="Site.analytics.track_event(this, 'Nav', 'Movies'); return false;">Filmes</a>
      </li>
      <li class="divider"></li>
      <li>
        <a href="{$aura->root}/{$PATH_PROFILE}" onClick="Site.analytics.track_event(this, 'Nav', 'Profile'); return false;">{$username} <span class="icon-cog icon-white"></span></a>
      </li>
      <li class="divider"></li>
      <li class="exit">
        <a href="{$aura->root}/{$PATH_LOGIN_OUT}" onClick=" Site.analytics.track_event(this, 'Login', 'Exit'); return false;">{$BUTTON_EXIT}</a>
      </li>
    </ul>
    {else}
    <ul class="unstyled unlogged">
      <li><a href="#" class="ico-login-facebook" id="facebook-login" onClick="_gaq.push(['_trackEvent', 'Login', 'Facebook']);">Facebook</a></li>
      <li><a href="{$aura->root}/signup/twitter" class="ico-login-twitter" onClick="Site.analytics.track_event(this, 'Login', 'Twitter'); return false;">Twitter</a></li>
      <li class="divider"></li>
      <li><a href="#" class="ico-login" data-controls-modal="login" data-backdrop="true" data-keyboard="true" rel="login" onClick="Site.analytics.track_event(this, 'Login', 'Default'); return false;">{$BUTTON_LOGIN}</a></li>
    </ul>
    {/if}
  </div>
{block name="body"}{/block}

{if !$logged}
<!-- Modal Login -->
<div id="login" class="modal hide fade">
    <form action="{$aura->root}/{$PATH_LOGIN}" method="post" name="login">
      <div class="modal-header">
        <a href="#" class="close">&times;</a>
        <h3>{$TITLE_LOGIN}</h3>
      </div>
      <div class="modal-body">
          <div class="alert-message warning hide">
            <p>{$ERROR_MESSAGE_LOGIN_DATA}</p>
          </div>
          <div class="alert-message error hide">
            <p>{$ERROR_MESSAGE_INVALID_LOGIN}</p>
          </div>
          <div class="alert-message success hide"><p></p></div>

          <p id="lgnp">
            {$LABEL_EMAIL}: 
            <input type="text" class="span4" name="login-email" /> <input type="password" class="span4" name="login-password" placeholder="{$PLACEHOLDER_PASSWORD}" />
            <input type="submit" class="btn primary" value="Ok" />
          </p>
          <p id="fpp" class="hide">
            {$LABEL_EMAIL}: 
            <input type="text" class="span6" name="email-forgot-password" /> <a href="{$aura->root}/{$PATH_JOIN}" class="btn secundary" rel="send-mail">{$BUTTON_CONTINUE}</a>
          </p>
      </div>
      <div class="modal-footer">
        <a href="{$aura->root}/{$PATH_FORGOT_PASSWORD}" class="btn secondary" id="forgot-password" onClick="_gaq.push(['_trackEvent', 'Login', 'Forgot Password']);">{$BUTTON_FORGOT_PASSWORD}</a>
        <a href="{$aura->root}/{$PATH_JOIN}" class="btn info" id="go-register" onClick="Site.analytics.track_event(this, 'Login', 'Join'); return false;">{$BUTTON_REGISTER}</a>
      </div>
    </form>
</div>
{else}
<!-- Modal Feedback -->
<div id="feedback" class="modal hide fade">
    <form action="{$aura->root}/{$PATH_FEEDBACK}" method="post" name="feedback" class="disabled_after_submit">
      <div class="modal-header">
        <a href="#" class="close">&times;</a>
        <h3>{$TITLE_FEEDBACK}</h3>
      </div>
      <div class="modal-body">
          <div class="alert-message hide"></div>
          <p><textarea name="comment" class="span9 hg1" placeholder="{$LABEL_FEEDBACK}"></textarea></p>
      </div>
      <div class="modal-footer">
        <input type="submit" value="Enviar" class="btn primary" data-label-submit="Aguarde, enviando mensagem" onClick="Site.analytics.track_event(this, 'Contact Us', 'Send'); return false;" />
      </div>
    </form>
</div>
{/if}

<div class="footer">
  <p>
    <a href="{$aura->root}/sobre" onClick="Site.analytics.track_event(this, 'Footer', 'About'); return false;">Sobre</a>
    <small class="divider">.</small>
    <a href="{$aura->root}/{$PATH_PRIVACY}" onClick="Site.analytics.track_event(this, 'Footer', 'Privacy'); return false;">{$LABEL_PRIVACY}</a>
    <small class="divider">.</small>
    <a href="{$aura->root}/{$PATH_TERMS}" onClick="Site.analytics.track_event(this, 'Footer', 'Terms'); return false;">{$LABEL_TERMS}</a>
    <small class="divider">.</small>
    <a href="http://facebook.com/pipocket" target="_blank" onClick="Site.analytics.track_event(this, 'Footer', 'On Facebook'); return false;">{$LABEL_ON_FACEBOOK}</a>
    <small class="divider">.</small>
    <a href="https://twitter.com/PipocketOficial" target="_blank" onClick="Site.analytics.track_event(this, 'Footer', 'On Twitter'); return false;">{$LABEL_ON_TWITTER}</a>
    {if $logged}
    <small class="divider">.</small>
    <a href="#" data-controls-modal="feedback" data-backdrop="true" data-keyboard="true" onClick="_gaq.push(['_trackEvent', 'Footer', 'Contact Us']);">Fale conosco</a>
    {/if}
    <br />
    © 2012 {$smarty.const.PROJECT_NAME}
  </p>
</div>

{block name="call-script"}{/block}
<script type="text/javascript" src="http://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript">
    {literal}
        window.fbAsyncInit = function() {
          FB.init({
            appId      : {/literal}'{$smarty.const.FACEBOOK_APP_ID}'{literal},
            status     : true, 
            cookie     : true,
            xfbml      : true,
            oauth      : true
          });
        };
    {/literal}

    $(function(){
      {if !$logged}
        Site.login();
        Site.forgot_password();
      {else}
        Site.feedback();
      {/if}

      {block name="script"}{/block}
    });
</script>
</body>
</html>