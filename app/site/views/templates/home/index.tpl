{block name="facebook-meta"}
   <meta property="og:title" content="{$smarty.const.PROJECT_NAME}"/>
   <meta property="og:type" content="website"/>
   <meta property="og:image" content="{$html->img}/pipocket-90x90.jpg"/>
   <meta property="og:description" content="Que tal compartilhar suas impressões sobre o filme que acabou de ver? Conte-nos sua experiência, compartilhe com seus amigos, e redefina a crítica de cinema."/>
{/block}

{block name="body"}
<div class="container">
	<div class="ranking">
		<h2><a href="{$aura->root}/busca/tipo/lideres-de-audiencia">{$TITLE_RANKING}</a></h2>
		{foreach from=$ranking item=movie}
		<dl class="movie-box {if isset($movie->last)}last{/if}">
			<dt>
				<span class="star {if $movie->rate == 1}half{else if $movie->rate < 2}off{/if}"></span>
				<span class="star {if $movie->rate == 3}half{else if $movie->rate < 4}off{/if}"></span>
				<span class="star {if $movie->rate == 5}half{else if $movie->rate < 6}off{/if}"></span>
				<span class="star {if $movie->rate == 7}half{else if $movie->rate < 8}off{/if}"></span>
				<span class="star {if $movie->rate == 9}half{else if $movie->rate < 10}off{/if}"></span>
			</dt>
			<dd>
				<span>
					<a href="{$aura->root}/{$PATH_MOVIE}/{$movie->key}">
						{if isset($movie->first)}<span class="lbl-first"></span>{/if}
						{if $movie->thumb}
						<img src="{$smarty.const.MEDIA_FILES_URL}/movies/{$movie->thumb}" alt="{$movie->name}">
						{else}
						<img src="{$html->img}/no-image-thumb1.jpg" alt="{$movie->name}">
						{/if}
					</a>
				</span>
				<h5>{$movie->name|upper}</h5>
			</dd>
		</dl>
		{/foreach}
	</div>

{if $logged}
	{include file="_Inc/widget-home-logged.tpl"}
{else}
	{include file="_Inc/widget-home-non-logged.tpl"}
{/if}
</div>

{if isset($redefine_success) OR isset($redefine_error)}
<!-- Modal confirmation -->
<div id="redefine-message" class="modal hide fade">
      <div class="modal-header">
        <a href="#" class="close">&times;</a>
        <h3>{$TITLE_COFIRMATION_REDEFINE}</h3>
      </div>
      <div class="modal-body">
      	{if isset($redefine_success) && $redefine_success}
		<div class="alert-message block-message success">
			<p><strong>Parabéns {$value_name|default:""}!</strong> Sua senha foi redefinida.</p>
		</div>
		{/if}

		{if isset($redefine_error) && $redefine_error}
		<div class="alert-message block-message error {if !$error}hide{/if}">
		  <p><strong>{$ERROR_MESSAGE_OPS}</strong></p>
		  <ul>
		  	<li id="name">{$ERROR_MESSAGE_REGISTER_NAME}</li>
		  </ul>
		</div>
		{/if}
      </div>
</div>
{/if}

{if isset($redefine_password)}
<!-- Modal Redefine passwotd -->
<div id="redefine-password" class="modal hide fade">
    <form action="{$aura->root}/{$PATH_REDEFINE_PASSWORD}" method="post" name="register">
      <div class="modal-header">
        <a href="#" class="close">&times;</a>
        <h3>{$TITLE_REDEFINE_PASSWORD}</h3>
      </div>
      <div class="modal-body">
          <div class="alert-message block-message error hide">
			  <p><strong>{$ERROR_MESSAGE_OPS}</strong></p>

			  <ul>
			  	<li id="password" class="hide">{$ERROR_MESSAGE_REGISTER_PASSWORD}</li>

			  	<li id="password-limit" {if !isset($error_maxlength_password) OR !$error_maxlength_password}class="hide"{/if}>{$ERROR_MESSAGE_REGISTER_PASSWORD_LIMIT}</li>

			  	<li id="confirm-password" {if !isset($error_password) OR !$error_password}class="hide"{/if}>{$ERROR_MESSAGE_REGISTER_CONFIRM_PASSWORD}</li>	
			  </ul>
			</div>

          <p id="lgnp">
            <input type="password" class="password" name="password" placeholder="{$LABEL_NEW_PASSWORD}" /> <input type="password" class="span4" name="confirm-password" placeholder="{$LABEL_CONFIRM_PASSWORD}" />
            <input type="submit" class="btn primary" value="Ok" />
          </p>
      </div>
    </form>
</div>
{/if}

{if isset($confirmation)}
<!-- Modal confirmation -->
<div id="confirmation" class="modal hide fade">
      <div class="modal-header">
        <a href="#" class="close">&times;</a>
        <h3>{$TITLE_COFIRMATION}</h3>
      </div>
      <div class="modal-body">
          <p>{$confirmation_message}</p>
      </div>
      {if $confirmation_login_btn}
      <div class="modal-footer">
        <a href="#" class="btn primary" rel="make-login">Fazer login!</a>
      </div>
      {/if}
</div>
{/if}
{/block}

{block name="call-script"}
<script type="text/javascript" src="{$html->js}/bootstrap-twipsy.js"></script>
<script type="text/javascript" src="{$html->js}/plugins/jcaption.min.js"></script>
{/block}

{block name="script"}
	Site.twipsy();
	Site.comments();
	Site.caption();
	{if isset($confirmation)}
	$('#confirmation').modal('show');
	Site.make_login();
	{/if}
	{if isset($redefine_password)}
	$('#redefine-password').modal({
		show: true,
		backdrop: true,
		keyboard: false
	});
	Site.register(true);
	{/if}
	{if isset($redefine_success) OR isset($redefine_error)}
	$('#redefine-message').modal({
		show: true,
		backdrop: true,
		keyboard: false
	});
	{/if}
{/block}