{block name="facebook-meta"}
   <meta property="og:title" content="{$smarty.const.PROJECT_NAME}"/>
   <meta property="og:type" content="website"/>
   <meta property="og:image" content="{$html->img}/pipocket-90x90.jpg"/>
   <meta property="og:description" content="Perfil de {$user->name}"/>
{/block}

{block name="body-class"}{if isset($user->movies_sidebar) AND $user->movies_sidebar}medium{else}nobar{/if}{/block}

{block name="body"}
<div class="container">
	{if isset($user->movies_sidebar) AND $user->movies_sidebar}
	<div class="row">
		<div class="span16 next-movies">
			<h2>{$TITLE_MY_FAVORITE_MOVIES}</h2>
			<ul class="unstyled">
				{foreach from=$user->movies_sidebar item=item key=key}
				<li {if $key == 5}class="last"{/if}>
					<a href="{$aura->root}/{$PATH_MOVIE}/{$item->key}" rel="twipsy" title="{$item->name}">
						<img src="{$smarty.const.MEDIA_FILES_URL}/movies/{$item->thumb}" alt="Thumb" />
					</a>
				</li>
				{/foreach}
			</ul>
		</div>
	</div>
	{/if}

	<div class="row movie-info">
		<div class="span-two-thirds">
			{if $user->profile}
			<ul class="pills">
			  <li class="active"><a href="{$aura->root}/{$PATH_PROFILE}" onClick="Site.analytics.track_event(this, 'Profile', 'Profile'); return false;">{$BUTTON_PROFILE}</a></li>
			  <li><a href="{$aura->root}/{$PATH_PROFILE_INFO}" onClick="Site.analytics.track_event(this, 'Profile', 'Adjustments'); return false;">{$BUTTON_ADJUSTMENTS}</a></li>
			</ul>
			{/if}

			<h2>{$user->name}</h2>
			<small class="info-normal">{$user->location}</small>

			{if $user->genders}
			<p><span class="break">{$LABEL_GENDERS}</span>
			{foreach from=$user->genders item=gender}
			<span class="list-item">
				{if $user->profile}
				<a class="close" href="#" rel="favorite" data-item-id="{$gender->gender_id}" data-item-type="gender" data-item-action="unfavorite">×</a>
				{/if}
				<a href="{$aura->root}/{$PATH_SEARCH}/genero/{$gender->description}">{$gender->description}</a>
			</span>
			{/foreach}
			</p>
			{/if}

			{if $user->actors}
			<p><span class="break">{$LABEL_ACTORS}</span> 
			{foreach from=$user->actors item=actor}
			<span class="list-item">
				{if $user->profile}
				<a class="close" href="#" rel="favorite" data-item-id="{$actor->actor_id}" data-item-type="actor" data-item-action="unfavorite">×</a>
				{/if}
				<a href="{$aura->root}/{$PATH_SEARCH}/elenco/{$actor->name}">{$actor->name}</a>
			</span>
			{/foreach}
			</p>
			{/if}

			{if $user->directors}
			<p><span class="break">{$LABEL_DIRECTORS}</span>
			{foreach from=$user->directors item=director}
			<span class="list-item">
				{if $user->profile}
				<a class="close" href="#" rel="favorite" data-item-id="{$director->director_id}" data-item-type="director" data-item-action="unfavorite">×</a>
				{/if}
				<a href="{$aura->root}/{$PATH_SEARCH}/direcao/{$director->name}">{$director->name}</a>
			</span>
			{/foreach}
			</p>
			{/if}

			{if $user->movies_favorite}
			<p><span class="break">{$LABEL_FAVORITE_MOVIES}</span> 
			{foreach from=$user->movies_favorite item=favorite key=key}
			<span class="list-item">
				<a href="{$aura->root}/{$PATH_MOVIE}/{$favorite->key}">{$favorite->name}</a>
			</span>
			{/foreach}
			</p>
			{/if}

			{if $user->next_movies}
			<p><span class="break">{$LABEL_NEXT_MOVIES}</span> 
			{foreach from=$user->next_movies item=next_movie key=key}
			<span class="list-item">
				<a href="{$aura->root}/{$PATH_MOVIE}/{$next_movie->key}">{$next_movie->name}</a>
			</span>
			{/foreach}
			</p>
			{/if}

			{if $user->movies}
			<p><span class="break">{$LABEL_WATCH_MOVIES}</span> 
			{foreach from=$user->movies item=movie key=key}
			<span class="list-item">
				<a href="{$aura->root}/{$PATH_MOVIE}/{$movie->key}">{$movie->name}</a>
			</span>
			{/foreach}
			</p>
			{/if}
		</div>
		<div class="span-one-third comments">
			<ul class="unstyled fr">
				{foreach from=$user->comments item=comment}
				<li><p>Sobre <a href="{$aura->root}/{$PATH_MOVIE}/{$comment->movie_data->key}" title="{$comment->movie_data->fullname}">{$comment->movie_data->name}</a> <span class="db">{$comment->description} <small class="db ar">{$LABEL_NOTE} {$comment->rate}</small></span></p></li>
				{/foreach}
			</ul>
		</div>
	</div>
</div>
{/block}

{block name="call-script"}
<script type="text/javascript" src="{$html->js}/bootstrap-twipsy.js"></script>
{/block}

{block name="script"}
	Site.twipsy();
	Site.comments();
	Site.favorite( true );
{/block}