{block name="body-class"}nobar{/block}

{block name="body"}
<div class="container">
	<div class="span16 settings">
		<ul class="pills">
		  <li id=""><a href="{$aura->root}/{$PATH_PROFILE}" onClick="Site.analytics.track_event(this, 'Profile', 'Profile'); return false;">{$BUTTON_PROFILE}</a></li>
		  <li class="active" id="informations"><a href="#" onClick="Site.analytics.track_event(this, 'Profile', 'Info'); return false;">{$BUTTON_INFO}</a></li>
		  <li id="settings"><a href="#" onClick="Site.analytics.track_event(this, 'Profile', 'Settings'); return false;">{$BUTTON_SETTINGS}</a></li>
		</ul>

		<div class="tab {if $success_preferences OR $twitter_success OR $twitter_error OR $foursquare_success OR $foursquare_error}hide{/if}" id="tab-informations">
			{if $success}
			<div class="alert-message block-message success">
				<a class="close" href="#">×</a>
				<p>{$SUCCESS_MESSAGE_SAVE_INFO}</p>
			</div>
			{/if}

			{if $error}
			<div class="alert-message block-message error">
			  <p><strong>{$ERROR_MESSAGE_OPS}</strong></p>

			  <ul>
			  	<li id="name" {if !isset($error_email_in_use) OR !$error_email_in_use}class="hide"{/if}>{$ERROR_MESSAGE_REGISTER_EMAIL_VERIFY}</li>
			  	<li id="password-limit" {if !isset($error_maxlength_password) OR !$error_maxlength_password}class="hide"{/if}>{$ERROR_MESSAGE_REGISTER_PASSWORD_LIMIT}</li>
			  </ul>
			</div>
			{/if}

			<form action="{$aura->root}/profile/basic" method="post" name="basic-info" class="disabled_after_submit">
				<fieldset>
					<div class="clearfix">
						<p>{$TEXT_REMEMBER_USER_FACEBOOK_TWITTER_LOGIN} <a href="#" data-controls-modal="data" data-backdrop="true" data-keyboard="true">{$LABEL_READ_MORE}</a></p>
					</div>
					<div class="clearfix">
						<label>{$FORMFIELD_NAME}</label>
						<div class="input">
							<input class="xlarge" type="text" value="{$informations->user_data->name}" disabled="true" />
						</div>
					</div>
					<div class="clearfix">
						<label>{$FORMFIELD_EMAIL}</label>
						<div class="input">
							<input class="xlarge" name="email" type="text" value="{$informations->user_data->email}" {if $informations->user_data->email}disabled="true"{/if}  />
						</div>
					</div>
					<div class="clearfix">
						<label>{$FORMFIELD_LOCATION}</label>
						<div class="input">
							<input type="text" name="location" class="xLarge" value="{$informations->user_data->location}" {if $informations->user_data->location}disabled="true"{/if}>
							<small class="info-normal">Cidade, País</small>
						</div>
					</div>
					<div class="clearfix">
						<label>{$FORMFIELD_GENDER}</label>
						<div class="input">
							<select name="gender">
								<option value="">{$FORMFIELD_GENDER_NONE}</option>
								<option value="M" {if $informations->user_data->gender == 'M'}selected="selected"{/if} >{$FORMFIELD_GENDER_MALE}</option>
								<option value="F" {if $informations->user_data->gender == 'F'}selected="selected"{/if} >{$FORMFIELD_GENDER_FEMALE}</option>
							</select>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>{$LABEL_SECURITY}</legend>
					<div class="clearfix">
						<label>{$LABEL_NEW_PASSWORD}</label>
						<div class="input">
							<input type="password" class="medium" name="password" />
							<span class="help-inline hide">{$ERROR_MESSAGE_REGISTER_PASSWORD_LIMIT}</span>
						</div>
					</div>
					<div class="clearfix">
						<label>{$LABEL_CONFIRM_PASSWORD}</label>
						<div class="input">
							<input type="password" class="medium" name="confirm-password" />
							<span class="help-inline hide">{$ERROR_MESSAGE_REGISTER_CONFIRM_PASSWORD}</span>
						</div>
					</div>
				</fieldset>
				<div class="actions">
					<input type="submit" class="btn primary" value="{$BUTTON_SAVE_CHANGES}" onClick="Site.analytics.track_event(this, 'Profile', 'Update Info'); return false;"/>
				</div>
			</form>
		</div>

		<div class="tab {if !$success_preferences AND !$twitter_success AND !$twitter_error AND !$foursquare_success AND !$foursquare_error}hide{/if}" id="tab-settings">
			<div class="alert-message block-message success hide">
				<a class="close" href="#">×</a>
				<p>{$SUCCESS_MESSAGE_SAVE_PREFERENCES}</p>
			</div>
			
			{if $twitter_error}
				{if !$twitter_denied}
				<div class="alert-message block-message error">
					<a class="close" href="#">×</a>
					<p>{$ERROR_MESSAGE_TWITTER_ID_ALREADY_EXISTS}</p>
				</div>
				{/if}
			{/if}

			<form action="{$aura->root}/profile/preferences" method="post" class="form-tacked" name="user-preferences">
				<fieldset>
					<div class="clearfix">
						<label>{$LABEL_PROFILE}</label>
						<div class="input">
							<ul class="inputs-list">
								<li>
									<label>
										<input type="checkbox" name="facebook_link" {if $informations->facebook_link}checked="true"{/if} /> 
										<span>{$TEXT_ALLOW_FACEBOOK_LINK}</span>
									</label>
								</li>
								<li>
									<label>
										<input type="checkbox" name="twitter_link" {if $informations->twitter_link}checked="true"{/if} /> 
										<span>{$TEXT_ALLOW_TWITTER_LINK}</span>
									</label>
								</li>
							</ul>
						</div>
					</div>
					<div class="clearfix">
						<label>{$LABEL_FACEBOOK}</label>
						<div class="input">
							<ul class="inputs-list">
								<li>
									<label>
										<input type="checkbox" name="facebook_post" id="facebook-authorize" {if $informations->facebook_post && $informations->facebook_post}checked="true"{/if} /> 
										<span>{$TEXT_ALLOW_POST_ON_FACEBOOK_WALL} <a href="#" data-controls-modal="postonsocialnetwork" data-backdrop="true" data-keyboard="true">{$LABEL_READ_MORE}</a></span>
									</label>
								</li>
								{if false}
								<li>
									<label>
										<input type="checkbox" name="facebook_default" {if $informations->facebook_default}checked="true"{/if} {if !$informations->facebook_post}disabled="true"{/if} /> 
										<span>{$TEXT_DEFAULT_POST_ON_FACEBOOK_WALL}</span>
									</label>
								</li>
								{/if}
							</ul>
						</div>
					</div>

					<div class="clearfix">
						<label>{$LABEL_TWITTER}</label>
						<div class="input">
							<ul class="inputs-list">
								<li>
									<label>
										<input type="checkbox" name="twitter_post" id="twitter-authorize" {if $informations->twitter_post OR $twitter_success}checked="true"{/if} /> 
										<span>{$TEXT_ALLOW_POST_ON_TWITTER} <a href="#" data-controls-modal="postonsocialnetwork" data-backdrop="true" data-keyboard="true">{$LABEL_READ_MORE}</a></span>
									</label>
								</li>
								{if false}
								<li>
									<label>
										<input type="checkbox" name="twitter_default" {if $informations->twitter_default && $informations->twitter_post}checked="true"{/if} {if !$informations->twitter_post}disabled="true"{/if} /> 
										<span>{$TEXT_DEFAULT_POST_ON_TWITTER}</span>
									</label>
								</li>
								{/if}
							</ul>
						</div>
					</div>
					
					{if false}
					<div class="clearfix">
						<label>{$LABEL_FOURSQUARE}</label>
						<div class="input">
							<ul class="inputs-list">
								<li>
									<label>
										<input type="checkbox" name="foursquare_last_checkins" id="foursquare-authorize" {if $informations->foursquare_last_checkins OR $foursquare_success}checked="true"{/if} /> 
										<span>{$TEXT_ALLOW_GET_LAST_CHECKINS} <a href="#" data-controls-modal="lastcheckins" data-backdrop="true" data-keyboard="true">{$LABEL_READ_MORE}</a></span>
									</label>
								</li>
							</ul>
						</div>
					</div>
					{/if}
					<div class="actions">
						
					</div>
				</fieldset>
			</form>
		</div>
	</div>

	<!-- Modal data -->
	<div id="data" class="modal hide fade">
	  <div class="modal-header">
	      <a href="#" class="close">&times;</a>
	      <h3>{$TITLE_COLLECT_MY_DATA}</h3>
	    </div>
	    <div class="modal-body">
	      <p>{$TEXT_COLLECT_MY_DATA_PART1}{$smarty.const.PROJECT_NAME}{$TEXT_COLLECT_MY_DATA_PART2}</p>
	    </div>
	</div>

	<!-- Modal social network -->
	<div id="postonsocialnetwork" class="modal hide fade">
	  <div class="modal-header">
	      <a href="#" class="close">&times;</a>
	      <h3>{$TITLE_POST_MY_ACTIONS}</h3>
	    </div>
	    <div class="modal-body">
	      <p>{$TEXT_POST_MY_ACTIONS}</p>
	    </div>
	</div>

	<!-- Modal Foursquare -->
	<div id="lastcheckins" class="modal hide fade">
	  <div class="modal-header">
	      <a href="#" class="close">&times;</a>
	      <h3>{$TITLE_COLLECT_MY_LAST_CHECKINS}</h3>
	    </div>
	    <div class="modal-body">
	      <p>{$TEXT_COLLECT_MY_LAST_CHECKINS_PART1}{$smarty.const.PROJECT_NAME}{$TEXT_COLLECT_MY_LAST_CHECKINS_PART2}</p>
	    </div>
	</div>
	
	<!-- Modal twitter authorize alert -->
	<div id="twitterauthorize" class="modal hide fade">
	  <div class="modal-header">
	      <a href="#" class="close">&times;</a>
	      <h3>{$username}</h3>
	    </div>
	    <div class="modal-body">
	      <p>{$TEXT_REDIRECT_TWITTER}</p>
	    </div>
	    <div class="modal-footer">
	       <a href="{$aura->root}/profile/twitter_authorize" class="btn primary">{$BUTTON_CONTINUE}</a>
	     </div>
	</div>

	<!-- Modal foursquare authorize alert -->
	<div id="foursquareauthorize" class="modal hide fade">
	  <div class="modal-header">
	      <a href="#" class="close">&times;</a>
	      <h3>{$username}</h3>
	    </div>
	    <div class="modal-body">
	      <p>{$TEXT_RERIRECT_FOURSQUARE}</p>
	    </div>
	    <div class="modal-footer">
	       <a href="{$aura->root}/profile/foursquare_authorize" class="btn primary">{$BUTTON_CONTINUE}</a>
	     </div>
	</div>
</div>
{/block}

{block name="script"}
	Site.basic_info();
	Site.tabs({if $success_preferences OR $twitter_success OR $twitter_error OR $foursquare_success OR $foursquare_error}'settings'{/if});
	Site.social_network_authorize();
	Site.user_set_preferences();
	{if $twitter_success OR $foursquare_success}
	Site.user_save_preferences();
	{/if}
{/block}