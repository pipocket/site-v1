{block name="facebook-meta"}
   <meta property="og:title" content="{$smarty.const.PROJECT_NAME}"/>
   <meta property="og:type" content="website"/>
   <meta property="og:image" content="{$html->img}/pipocket-90x90.jpg"/>
   <meta property="og:description" content="Que tal compartilhar suas impressões sobre o filme que acabou de ver? Conte-nos sua experiência, compartilhe com seus amigos, e redefina a crítica de cinema."/>
{/block}

{block name="body-class"}nobar{/block}

{block name="body"}
<div class="container">
	<div class="span16 terms-privacy">
		<h1>{$LABEL_TERMS}</h1>
		<br/>

		<p>Este documento define os termos de uso do site Pipocket.</p>

		<p>O Site tem como propósito ajudar pessoas a escolher, avaliar, e compartilhar opiniões sobre filmes.</p>
		<br/>

		<h3>1. Conteúdo disponível no site</h3>

		<p>
			<strong>1.1</strong> Todo conteúdo relacionado a filme, é retirado de fontes seguras como o próprio site do filme, ou de sua produtora.
		</p>
		<p>
			<strong>1.2</strong> A descrição do filme, ou sinopse, quando não disponível nas fontes seguras, é escrita pela equipe do Pipocket.
		</p>
		<p>
			<strong>1.3</strong> Todos os comentários feitos pelos usuários, refletem somente sua opinião e o Pipocket não se responsabilliza por possíveis danos decorrentes de tal conteúdo.
		</p>
		<p>
			<strong>1.4</strong> Todo o conteúdo disponível no site, incluindo os dados que aparecem no perfil do usuário, é indexado pelos mecanismos de busca.
		</p>
		<p>
			<strong>1.5</strong> O conteúdo cujo usuário compartilha nas redes sociais, é de inteira responsabilidade do mesmo.
		</p>
		<br/>

		<h3>2. Propriedade Intelectual</h3>

		<p>
			<strong>2.1</strong> O Pipocket é o exclusivo titular dos direitos de propriedade intelectual sobre:
		</p>
		<p>
			i. O conteúdo deste site, com exceção daqueles enviados pelo usuário como: Dados cadastrais e comentários sobre filmes;
		</p>
		<p>
			ii. A estatística gerada pelas notas dadas aos filmes;
		</p>
		<br/>

		<h3>3. Compartilhamento de conteúdo</h3>

		<p>
			<strong>3.1</strong> O usuário não poderá utilizar o site para:
		</p>
		<p>
			i. Fazer propaganda, própria ou de terceiros;
		</p>
		<p>
			ii. Ameaçar, importunar, pertubar e/ou ofender outros usuários;
		</p>
		<p>
			iii. Praticar atos discriminatórios, seja em razão de sexo, raça, religião, credo, ou idade;
		</p>
		<br/>

		<h3>4. Remoção de conteúdo</h3>

		<p>
			<strong>4.1</strong> Caso seja contatado conteúdo que viole a disposição legal, ou deste Termo do uso, o Pipocket poderá, a seu livre critério, e sem aviso prévio ao usuário, remover tal conteúdo.
		</p>
		<br/>

		<h3>5. Cadastro, Login e Segurança</h3>

		<p>
			<strong>5.1</strong> O usuário, durante o cadastro, fornece apenas nome, senha e endereço de e-mail.
		</p>
		<p>
			<strong>5.2</strong> O usuário, após o cadastro pode atualizar informações como sexo, cidade e país onde reside.
		</p>
		<p>
			<strong>5.3</strong> O usuário pode optar por usar o Facebook® ou Twitter® para fazer login no site, neste caso um registro é gerado automaticamente, apenas para manter o vínculo entre usuário e seu respectivo conteúdo.
		</p>
		<p>
			<strong>5.4</strong> É vedado ao usuário a prática de quaisquer atos em nome de terceiros, seja fisíca ou jurídica.
		</p>
		<p>
			<strong>5.5</strong> O usuário deverá zelar pela segurança de sua senha.
		</p>
		<p>
			<strong>5.6</strong> O Pipocket usa encriptação de 128bits para proteger a senha do usuário. 
		</p>
		<br/>

		<h3>6. Responsabilidade</h3>

		<p>
			<strong>6.1</strong> O usuário assume todos os ônus e responsabilidades decorrentes de seus atos, ainda, que por terceiros, por meio de uso de seu login e senha.
		</p>
		<br/>

		<h3>7. Navegação</h3>

		<p>
			<strong>7.1</strong> O usuário compromete-se a utilizar a versão mais recente de seu navegador de internet, para melhor experiência de uso do site.
		</p>
		<p>
			<strong>7.2</strong> O usuário poderá, por meio de links, ser redirecionado para ambientes fora do Pipocket, tais como perfil de usuário no Facebook ou Twitter.
		</p>
		<br/>

		<a name="avaliacao"><!-- Anchor --></a>
		<h3>8. Avaliação de filmes</h3>

		<p>
			<strong>8.1</strong> Os filmes são liberados para avaliação 10 (dez) dias antes de sua data de estréia base<sup>1</sup>, pelos seguintes motivos:
		</p>
		<p>
			i. Pré-estréia fechada (evento promovido por empresas ligadas diretamente ou indiretamente ao filme/produtora);
		</p>
		<p>
			ii. Pré-estréia;
		</p>
		<p>
			iii. Casos em que um mesmo filme estréia em datas diferentes, nos diversos cinemas do país;
		</p>
		<p>
			<i><strong><sup>1</sup></strong> A data de estréia "base" é a data praticada nos principais cinemas</i>
		</p>
		<br/>

		<h3>9. Disposições gerais</h3>

		<p>
			<strong>9.1</strong> O usuário autoriza a exibição de conteúdo enviado ao site, na página inicial, ou em outras páginas, cujas podem ser visualizadas por qualquer usuário, mesmo não tendo efetuado login:
		</p>
		<p>
			i. Foto e nome;
		</p>
		<p>
			ii. Atividades como: Voto, comentário e compartilhamento;
		</p>

		<p>
			<strong>9.2</strong> As imagens dos filmes, ou de serviços, exibidos no Site, são meramente ilustrativas.
		</p>
		<br/>

		<h3>10. Modificações no Termo de uso</h3>

		<p>
			<strong>10.1</strong> Estes Termos de Uso poderão ser atualizados ou modificados a qualquer momento.
		</p>
		<p>
			<strong>10.2</strong> Caberá ao usuário manter-se atualizado com todas as disposições constantes nos Termos de Uso.
		</p>
		<br/><br/><br/>
		<p><i>Última atualização: 17/07/2012 às 12:03</i></p>
	</div>
</div>
{/block}