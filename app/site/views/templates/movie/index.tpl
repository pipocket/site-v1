{block name="facebook-meta"}
  <meta property="og:title" content="{$movie->name|escape}"/>
  <meta property="fb:app_id" content="{$smarty.const.FACEBOOK_APP_ID}" />
  <meta property="og:locale" content="pt_BR"/>
  <meta property="og:type" content="movie"/>
  <meta property="og:site_name" content="{$smarty.const.PROJECT_NAME}"/>
  <meta property="og:image" content="{$smarty.const.MEDIA_FILES_URL}/movies/{$movie->thumb_facebook}"/>
  <meta property="og:description" content="{$movie->storyline|escape}"/>
{/block}

{block name='title'}{$movie->name} - {/block}

{block name="body"}
<div class="container">
	<div class="row gallery">
		<ul class="unstyled" id="slideshow">
			{if $movie->poster|@count != 0}
			{foreach from=$movie->poster item=poster}
			<li><img src="{$smarty.const.MEDIA_FILES_URL}/movies/{$poster->image}" alt=""></li>
			{/foreach}
			{else}
			<li><img src="{$html->img}/no-image-banner.jpg" alt=""></li>
			{/if}
		</ul>
	</div>
	<div class="row">
		<div class="span16 ac stars-line">
			<span class="star {if $movie->rate == 1}half{else if $movie->rate < 2}off{/if}"></span>
			<span class="star {if $movie->rate == 3}half{else if $movie->rate < 4}off{/if}"></span>
			<span class="star {if $movie->rate == 5}half{else if $movie->rate < 6}off{/if}"></span>
			<span class="star {if $movie->rate == 7}half{else if $movie->rate < 8}off{/if}"></span>
			<span class="star {if $movie->rate == 9}half{else if $movie->rate < 10}off{/if}"></span>

			{if $movie->voted}
			<a class="over-voted" rel="twipsy" title="{$LABEL_HIS_EVALUATION_WAS} {$movie->voted}" data-placement="below"></a>
			{else if $logged && $movie->open}
			<a class="over-voted pointer" rel="twipsy" title="{$LABEL_GIVE_YOUR_EVALUATION}" data-offset="5" data-placement="below" data-controls-modal="vote" data-backdrop="true" data-keyboard="true" onClick="Site.analytics.track_event(this, 'Vote', 'Modal', false);" href="#"></a>
			{else if $logged && !$movie->open}
			<!-- <a class="over-voted" rel="twipsy" title="{$LABEL_THIS_MOVIE_NOT_OPENED} {$movie->release_date}" data-offset="5" data-placement="below"></a> -->
			<a class="over-voted" rel="popover" data-content="Este filme ainda não esta liberado para voto. <a href='{$aura->root}/termos#avaliacao'>Entenda o porquê</a>." data-offset="5" data-placement="below"></a>
			{else}
			<a class="over-voted" rel="popover" data-content="<a href='{$aura->root}/faca-parte'>Registre-se!</a> é rápido e fácil. Ou <a href='#' data-controls-modal='login' data-backdrop='true' data-keyboard='true'>faça login</a> para votar." data-offset="5" data-placement="below"></a>
			{/if}
		</div>
	</div>

	<!--
	<div class="row">
		<div class="span16">
			<div class="fb-like {if $movie->voted}movie-voted{/if}" data-href="{$current_url}" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false"></div>
		</div>
	</div>
	-->

	<div class="row movie-info">
		<div class="span-two-thirds">
			<h2>{$movie->name}</h2>
			<small class="info">{$movie->original_name|@upper} - {$movie->year} - {$movie->origin}</small>

			<p class="horizontal-space"></p>
			
			<p>
				<span class="icon-calendar"></span> {$LABEL_MOVIE_PREMIERE} {$movie->release_date} <span class="space2"></span>
				<span class="icon-eye-open" title="Classificação etária"></span> {if $movie->age == 'L'}Livre{else if $movie->age == 1}Desconhecido{else}{$movie->age} anos{/if} <span class="space2"></span>
				<span class="icon-time" title="Duração"></span> {if $movie->time}{$movie->time} min.{else}Desconhecido{/if} <span class="space2"></span>

				{if $movie->viewed}
					{if $movie->favorite}
					<span class="icon-star"></span> <a href="#" class="discrete-link strong" rel="favorite" data-item-id="{$movie->id}" data-item-type="movie" data-item-action="unfavorite">{$BUTTON_UNFAVORITE}</a>
					{else}
					<span class="icon-star-empty"></span> <a href="#" class="discrete-link strong" rel="favorite" data-item-id="{$movie->id}" data-item-type="movie" data-item-action="favorite">{$BUTTON_FAVORITE}</a>
					{/if}
				{else}
					{if $logged}
					<span class="check">
						{if $movie->next}
						<span class="icon-check"></span> <a href="#" class="discrete-link strong" rel="next-movie" data-item-id="{$movie->id}" data-item-action="uncheck">{$BUTTON_UNMARK_AS_THE_NEXT}</a>
						{else}
						<a href="#" class="discrete-link strong" rel="next-movie" data-item-id="{$movie->id}" data-item-action="check">{$BUTTON_MARK_AS_THE_NEXT}</a>
						{/if}
					</span>
					{/if}
				{/if}
			</p>

			<p class="horizontal-space"></p>

			<p><span class="break">{$LABEL_STORYLINE}</span> {$movie->storyline|nl2br}</p>

			<p class="horizontal-space"></p>

			<p>
				<span class="break">{$LABEL_GENDER}</span>
				{foreach from=$movie->genders item=gender key=key}
					{if $logged}
						{if isset($gender->favorite)}
							{assign var='data_content' value="<span class='icon-star'></span> <a href='#' rel='favorite' data-item-type='gender' data-item-action='unfavorite' data-item-id='{$gender->id}'>{$BUTTON_UNFAVORITE}</a> {$gender->description}"}

							<span class="icon-star"></span>
						{else}
							{assign var='data_content' value="<span class='icon-star-empty'></span> <a href='#' rel='favorite' data-item-type='gender' data-item-action='favorite' data-item-id='{$gender->id}'>{$BUTTON_FAVORITE}</a> {$gender->description}"}
						{/if}

						<a href="#" rel="popover" data-item-id="{$gender->id}" data-content="{$data_content}">{$gender->description}</a>{else}{$gender->description}{/if}{if $key+1 != $movie->genders|count}, {/if}
				{/foreach}
			</p>

			<p class="horizontal-space"></p>

			<p>
				<span class="break">{$LABEL_DIRECTOR}</span> 
				{foreach from=$movie->directors item=director key=key}
					{if $logged}
						{if isset($director->favorite)}
						<span class="icon-star"></span>
							{assign var='data_content' value="<span class='icon-star'></span> <a href='#' rel='favorite' data-item-type='director' data-item-action='unfavorite' data-item-id='{$director->id}'>{$BUTTON_UNFAVORITE}</a> {$director->name}"}
						{else}
							{assign var='data_content' value="<span class='icon-star-empty'></span> <a href='#' rel='favorite' data-item-type='director' data-item-action='favorite' data-item-id='{$director->id}'>{$BUTTON_FAVORITE}</a> {$director->name}"}
						{/if}

						<a href="#" rel="popover" data-item-id="{$director->id}" data-content="{$data_content}">{$director->name}</a>{else}{$director->name}{/if}{if $key+1 != $movie->directors|count}, {/if}
				{/foreach}
			</p>

			<p>
				<span class="break">{$LABEL_MAIN_ACTORS}</span> 
				{foreach from=$movie->actors item=actor key=key}
					{if $logged}
						{if isset($actor->favorite)}
						<span class="icon-star"></span>
						{assign var='data_content' value="<span class='icon-star'></span> <a href='#' rel='favorite' data-item-type='actor' data-item-action='unfavorite' data-item-id='{$actor->id}'>{$BUTTON_UNFAVORITE}</a> {$actor->name}"}
						{else}
						{assign var='data_content' value="<span class='icon-star-empty'></span> <a href='#' rel='favorite' data-item-type='actor' data-item-action='favorite' data-item-id='{$actor->id}'>{$BUTTON_FAVORITE}</a> {$actor->name}"}
						{/if}

						<a href="#" rel="popover" data-item-id="{$actor->id}" data-content="{$data_content}">{$actor->name}</a>{else}{$actor->name}{/if}{if $key+1 != $movie->actors|count}, {/if}
				{/foreach}
			</p>
		</div>
		<div class="span-one-third comments">
			<ul class="unstyled fr">
				{foreach from=$movie->comments item=comment}
				<li><p><a href="{$aura->root}/{$PATH_PROFILE}/{$comment->user_id}">{$comment->user_data->name}</a>: {$comment->description} <small class="db ar">{$LABEL_NOTE} {$comment->rate}</small></p></li>
				{foreachelse}
				<li><p>{$LABEL_NO_COMMENTS}</p></li>
				{/foreach}
			</ul>
		</div>
	</div>
</div>

{if $logged}
<!-- Modal vote -->
<div id="vote" class="modal hide fade">
	<form action="{$aura->root}/{$PATH_FORM_VOTE}" name="vote" method="post">
	  	<div class="modal-header">
	      <a href="#" class="close">&times;</a>
	      <h3>{$TITLE_JOIN}</h3>
	    </div>
	    <div class="modal-body">
	    	<div class="alert-message warning hide">
			  <p>{$ERROR_MESSAGE_FORGOT_EVALUATION_MOVIE}</p>
			</div>
			<div class="alert-message error hide">
			  <p>{$ERROR_MESSAGE_EVALUATION_MOVIE}</p>
			</div>
			<div class="alert-message error hide voted">
			  <p>{$ERROR_MESSAGE_ALREADY_VOTED}</p>
			</div>

			<p class="upper"><strong>{$movie->name}</strong></p>
			<p>
				<span id="rating"></span>&nbsp;<small class="info" id="target"></small>
			</p>
			<p>
				<textarea name="comment" class="span9 hg1" data-maxlength="139" placeholder="{$LABEL_ANY_COMMENT}"></textarea>
				<br/><small class="info-normal space" id="location"></small>
			</p>
			<p>
				<input type="checkbox" name="facebook" {if $movie->user_preferences->facebook_post == 0}disabled="true"{elseif $movie->user_preferences->facebook_default ==1}checked="checked"{/if} /> Facebook &nbsp;&nbsp;
				<input type="checkbox" name="twitter" {if $movie->user_preferences->twitter_post == 0}disabled="true"{elseif $movie->user_preferences->twitter_default}checked="checked"{/if} /> Twitter
			</p>
			{if $movie->user_preferences->foursquare_last_checkins}
			<div class="scroll">
				<table class="bordered-table zebra-striped" id="last-checkins">
				<thead>
					<tr>
						<th class="ac"><a href="#" class="btn info" rel="load">{$BUTTON_SHOW_LAST_CHECKINS_ON_FOURSQUARE}</a></th>
					</tr>
				</thead>
				<tbody class="hide">
				</tbody>
			</table>
			</div>
			{/if}
	    </div>
	    <div class="modal-footer">
	      <input type="hidden" name="movie_id" value="{$movie->id|base64_encode}" />
	      <input type="submit" class="btn primary" value="{$BUTTON_OK}">
	      <input type="button" class="btn secondary close-modal" value="{$BUTTON_CANCEL}">
	    </div>
    </form>
</div>
{/if}
{/block}

{block name="call-script"}
<script type="text/javascript" src="{$html->js}/bootstrap-twipsy.js"></script>
<script type="text/javascript" src="{$html->js}/bootstrap-popover.js"></script>
<script type="text/javascript" src="{$html->js}/plugins/jquery.cycle.lite.js"></script>
<script type="text/javascript" src="{$html->js}/plugins/jquery.raty.min.js"></script>
<script type="text/javascript" src="{$html->js}/plugins/jquery.limit-1.2.source.js"></script>
{/block}

{block name="script"}
	Site.comments();
	Site.slideshow();
	Site.rating();
	Site.vote();
	Site.twipsy();
	Site.last_checkins();
	Site.person_actions();
	Site.favorite();
	Site.next_movie();
	Site.slide_poster_load();
{/block}