{block name="facebook-meta"}
   <meta property="og:title" content="{$smarty.const.PROJECT_NAME}"/>
   <meta property="og:type" content="website"/>
   <meta property="og:image" content="{$html->img}/pipocket-90x90.jpg"/>
   <meta property="og:description" content="Que tal compartilhar suas impressões sobre o filme que acabou de ver? Conte-nos sua experiência, compartilhe com seus amigos, e redefina a crítica de cinema."/>
{/block}

{block name="body-class"}small{/block}

{block name="body"}
<div class="container">
	<div class="row filter">
		<div class="span16">
			<form action="{$aura->root}/{$PATH_SEARCH}" class="framework-filter">
				<input type="text" name="{$FORMNAME_TITLE}" class="span4" placeholder="{$PLACEHOLDER_TITLE_OF_MOVIE}" value="{$title}"/>
				<input type="text" name="{$FORMNAME_CAST}" class="span4" placeholder="{$PLACEHOLDER_CAST}" value="{$cast}"/>
				<input type="text" name="{$FORMNAME_DIRECTOR}" class="span4" placeholder="{$PLACEHOLDER_DIRECTOR}" value="{$director}"/>
				<select name="{$FORMNAME_TYPE}">
					<option value="todos">{$FORMOPTION_ALL_MOVIES}</option>
					<option value="lideres-de-audiencia" {if $type == 'lideres-de-audiencia'}selected="selected"{/if}>{$FORMFIELD_SEARCH_RANKING}</option>
					<option value="estreias" {if $type == 'estreias'}selected="selected"{/if}>{$FORMFIELD_SEARCH_OPEN}</option>
					<option value="em-cartaz" {if $type == 'em-cartaz'}selected="selected"{/if}>{$FORMFIELD_SEARCH_IN_MTHEATER}</option>
					<option value="arquivados" {if $type == 'arquivados'}selected="selected"{/if}>{$FORMFIELD_SEARCH_FILED}</option>
				</select>
				<input type="submit" value="{$BUTTON_SEARCH}" class="btn primary" onClick="Site.analytics.track_event(this, 'Search', 'Filter'); return false;" />
			</form>
		</div>
	</div>

	<div class="row result">
		<div class="span16">
			{if $type == 'lideres-de-audiencia'}
			<h2>{$TITLE_RANKING}</h2>
			{else if $type == 'estreias'}
			<h2>{$TITLE_MOVIE_PREMIER}</h2>
			{else if $title OR $cast OR $director OR $gender OR $type}
			<h2><span>{if $result|@count > 0}{$result|@count}{else}{$TEXT_NOTHING}{/if}</span> {$LABEL_MOVIE|lower}{if $result|@count> 1}s{/if} {$LABEL_FOUND|lower}{if $result|@count> 1}s{/if}</h2>
			{else}
			<h2>{$TITLE_SEARCH}</h2>
			<p>{$TEXT_SEARCH_USE_FILTER}</p>
			{/if}
			
			{foreach from=$result item=movie key=i}
			<dl class="movie-box search-result {if ($i+1)%5 == 0}last{/if}">
				<dt>
					<span class="star {if $movie->rate == 1}half{else if $movie->rate < 2}off{/if}"></span>
					<span class="star {if $movie->rate == 3}half{else if $movie->rate < 4}off{/if}"></span>
					<span class="star {if $movie->rate == 5}half{else if $movie->rate < 6}off{/if}"></span>
					<span class="star {if $movie->rate == 7}half{else if $movie->rate < 8}off{/if}"></span>
					<span class="star {if $movie->rate == 9}half{else if $movie->rate < 10}off{/if}"></span>
				</dt>
				<dd>
					<span>
						<a href="{$aura->root}/{$PATH_MOVIE}/{$movie->key}" rel="twipsy" title="{$movie->name}" data-offset="{if $movie->current}25{else}10{/if}">
							{if $movie->thumb}
							<img src="{$smarty.const.MEDIA_FILES_URL}/movies/{$movie->thumb}" alt="" width="123" height="174">
							{else}
							<img src="{$html->img}/no-image-poster.jpg" alt="{$movie->name}">
							{/if}
						</a>
					</span>
					<h5>{$movie->current|upper}</h5>
				</dd>
			</dl>
			{/foreach}
		</div>
	</div>
</div>
{/block}

{block name="call-script"}
<script type="text/javascript" src="{$html->js}/bootstrap-twipsy.js"></script>
{/block}

{block name="script"}
	Site.twipsy();
{/block}