$(function(){
	Site._init();
});

var Vars = {
	path_title: '',
	path_cast: '',
	path_gender: '',
	path_director: '',
	checkins: ''
}

var Site = {

	_init: function() {
		try {
			this._placeholder();
			this.search();
			this.close_modal();
			this.maxlength();
			this.close_alert_message();
			this.disabled_submit();
		} catch (e) {
			alert('Error: ' + e.description);
		}
	},

	_placeholder: function() {
		if($.browser.msie) {
			$('input[type=password].insertLabel').each(function(){
				$('<label class=\'form\'>' + $(this).attr('placeholder') + '</label>').insertBefore($(this));
			});
		}
	},

	search: function() {
		$('form[name=search]').submit(function(){
			var action = $(this).attr('action');
			var input = $(this).find('input[type=text]');

			if ( !input.val() )
				$(this).find('input[type=hidden]').attr('data-framework-get', 'true');
		});
	},

	disabled_submit: function() {
		$('form.disabled_after_submit').submit(function(){
			var text = $(':submit', this).data('label-submit');
				text = text ? text : 'Aguarde, salvando alterações';

			$(':submit', this).attr('disabled', 'disabled').val(text);
		});
	},

	close_alert_message: function(){
		$('div.alert-message a.close').click(function(e){
			e.preventDefault();

			$(this).parent().fadeOut('fast', function(){
				$(this).remove();
			});
		});
	},

	maxlength: function() {
		$( '[data-maxlength]' ).each( function() {
			$(this).limit( $(this).data('maxlength'), null );
		});
	},

	twipsy: function() {
		$("a[rel=twipsy]").twipsy({
			live: true,
			placement: 'below',
			html: true
		});
	},

	close_modal: function() {
		$('.close-modal').click(function(){
			var modal = $(this).parent().parent().parent().attr('id');
			
			$('#' + modal).modal('hide');
		});
	},

	comments: function() {
		$('div.comments ul li a[rel=more], ul.home-comments-list li a[rel=more]').click(function(e){
			e.preventDefault();
			$(this).next().fadeIn('slow');
			$(this).remove();
		});
	},

	caption: function() {
		$("div.premieres ul li img").jcaption({
			
    		copyStyle: true,
			animate: true,
			show: {height: "show"},
			hide: {height: "hide"}
		});
	},

	tabs: function(tab) {
		if (tab) {
			var tab_active = $('ul.pills li.active').attr('id');

			$('ul.pills li').removeClass('active');
			$('ul.pills li#' + tab).addClass('active');

			$('div#tab-' + tab_active).fadeOut('fast', function(){
				$('div#tab-' + tab).fadeIn('fast');
			});
		}

		$('ul.pills li a').click(function(e){
			var tab = $(this).parent().attr('id');
			var tab_active = $(this).parent().parent().find('li.active').attr('id');

			if (tab)
				e.preventDefault();

			$('ul.pills li').removeClass('active');
			$(this).parent().addClass('active');

			$('div#tab-' + tab_active).fadeOut('fast', function(){
				$('div#tab-' + tab).fadeIn('fast');
			});
		});
	},

	slideshow: function() {
		$('#slideshow').cycle(function(){
			fx: 'fade'
		});
	},

	rating: function() {
		$('#rating').raty({
			path: aura.web.img,
			starOn: '/ico/star-on.png',
			starOff: '/ico/star-off.png',
			starHalf: '/ico/star-half.png',
			hintList: ['', '', '', '', ''],
			half: true,
			click: function(score, evt) {
				score *= 2;
				$('#target').html('Nota ' + score);
			}
		});
	},

	user_set_preferences: function() {
		$("input[name='facebook_link'], input[name='twitter_link'], input[name='twitter_default'], input[name='facebook_default']").click(function(e){
			Site.user_save_preferences();
		});
	},

	social_network_authorize: function() {
		$('input#facebook-authorize').click(function(e){
			var $this = $(this);

			if($this.is(':checked')) {
				FB.login(function(response) { 
					FB.api('/me/permissions', function(response) {
					  if(response.data[0].publish_stream == 1) {
					  	$("form[name='user-preferences'] fieldset").append("<div><input type='hidden' name='facebook_update' value='true' /></div>");
					  	Site.user_save_preferences();
					  }
					  else
					  	$this.removeAttr('checked');
					});
				}, {scope: 'publish_stream'});
			}
			else
				Site.user_save_preferences();
		});

		$('input#twitter-authorize').click(function(e){
			var $this = $(this);

			if($this.is(':checked')) {
				$('#twitterauthorize').modal({
					'show': true,
					backdrop: true,
					keyboard: false
				});

				$('#twitterauthorize').on('hidden', function() {
					$('input#twitter-authorize').removeAttr('checked');
				});
			}
			else
				Site.user_save_preferences();
		});

		$('input#foursquare-authorize').click(function(e){
			var $this = $(this);

			if($this.is(':checked')) {
				$('#foursquareauthorize').modal({
					'show': true,
					backdrop: true,
					keyboard: false
				});

				$('#foursquareauthorize').on('hidden', function() {
					$('input#foursquare-authorize').removeAttr('checked');
				});
			}
			else
				Site.user_save_preferences();
		});
	},

	user_save_preferences: function() {
		var form = $("form[name='user-preferences']");
		form.find('div.actions').html('<img src=\'' + aura.web.img + '/ico/ajax-loader.gif\' /> <small class=\'info loading-message\'>Atualizando preferências</small>');

		$.post(
			form.attr('action'),
			form.serialize(),
			function (response) {
				if (response) {
					form.parent().find('div.alert-message.block-message.success').fadeIn('fast');
				}

				form.find('div.actions').html('');
			}, 'json'
		);
	},

	validate: function(form, type, condition) {
		if (type == 'warning') {
			if (condition) {
				form.find('div.modal-body div.alert-message.warning').fadeIn('fast');
				return false;
			}
			else {
				form.find('div.modal-body div.alert-message.warning').hide();
			}

			form.find('div.modal-footer').append("<img src='" + aura.web.img + "/ico/ajax-loader.gif' class='fr' />");
			form.find('input[type=submit]').attr('disabled', 'disabled');

			return true;
		}
		else if (type == 'error') {
			var status = false;

			if (condition == 'false')
				form.find('div.modal-body div.alert-message.error').fadeIn('fast');
			else if (condition == 'voted')
				form.find('div.modal-body div.alert-message.error.voted').fadeIn('fast');
			else {
				form.find('div.modal-body div.alert-message.error').fadeOut('fast');
				status = true;
			}

			form.find('div.modal-footer img').remove();
			form.find('input[type=submit]').removeAttr('disabled');

			return status;
		}
	},

	register: function( only_password ) {
		var email_reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		var $this = $('form[name=register]');
		var email = $this.find('input[name=email]');

		$this.submit(function(){
			var error = false;
			var name = $this.find('input[name=name]');
			var email = $this.find('input[name=email]');
			var password = $this.find('input[name=password]');
			var confirm_password = $this.find('input[name=confirm-password]');

			if ( !only_password ) {
				if (!name.val()) {
					error = true;
					name.addClass('error');
					$this.parent().find('div.alert-message.error ul li#name').show();
				}
				else {
					name.removeClass('error');
					$this.parent().find('div.alert-message.error ul li#name').hide();
				}

				if (!email.val() || !email_reg.test(email.val())) {
					error = true;
					email.addClass('error');
					$this.parent().find('div.alert-message.error ul li#email').show();
				}
				else {
					email.removeClass('error');
					$this.parent().find('div.alert-message.error ul li#email').hide();
				}
			}

			if (!password.val()) {
				error = true;
				password.addClass('error');
				$this.parent().find('div.alert-message.error ul li#password').show();
			}
			else {
				password.removeClass('error');
				$this.parent().find('div.alert-message.error ul li#password').hide();
			}

			if (password.val().length < 6) {
				error = true;
				password.addClass('error');
				$this.parent().find('div.alert-message.error ul li#password-limit').show();
			}
			else {
				password.removeClass('error');
				$this.parent().find('div.alert-message.error ul li#password-limit').hide();
			}
			

			if (password.val() != confirm_password.val()) {
				error = true;
				confirm_password.addClass('error');
				$this.parent().find('div.alert-message.error ul li#confirm-password').show();
			}
			else {
				confirm_password.removeClass('error');
				$this.parent().find('div.alert-message.error ul li#confirm-password').hide();
			}

			if (error) {
				$this.parent().find('div.alert-message.error').show();
				return false;
			}
			else {
				$this.parent().find('div.alert-message.error').hide();

				// only_password -> redefine password
				if (!only_password) {
					$this.parent().find('div.alert-message.info').show()
						.find('strong')
						.css({'position':'relative', 'top':'-7px', 'left':'5px'})
						.html('Aguarde, enviando informações...');

						$.post(
							aura.path.root + '/signup/register',
							$this.serialize(),
							function( response ) {
								if ( !response.status ) {
									$('div.alert-message.block-message.error').removeClass('hide').show();

									if ( response.register_validate_email )
										$('div.alert-message.block-message.error ul li#email').show();
									if ( response.register_validate_name )
										$('div.alert-message.block-message.error ul li#name').show();
									if ( response.register_validate_password )
										$('div.alert-message.block-message.error ul li#confirm-password').show();
									if ( response.register_validate_maxlength_password )
										$('div.alert-message.block-message.error ul li#password-limit').show();
								}
								else {
									$('div.alert-message.block-message.error').hide();
									$('div.alert-message.block-message.success')
										.removeClass('hide')
										.show()
										.html("<p><strong>Parabéns!</strong> Seu registro foi efetuado com sucesso, verifique seu e-mail e ative sua conta.</p>");

									name.val('');
									email.val('');
									password.val('');
									confirm_password.val('');
								}

								$this.parent().find('div.alert-message.info').hide().find('strong').html('');
							}
						)
				}
				
			}

			if (!only_password)
				return false;
		});

		email.blur(function(){
			var status = false;

			if (email.val() && email_reg.test(email.val())) {
				email.removeClass('error');
				$this.parent().find('div.alert-message.error ul li#email').hide();

				$this.parent().find('div.alert-message.error ul li').each(function(){
					if ($(this).hasClass('hide'))
						status = true;
				});

				if (!status)
					$this.parent().find('div.alert-message.error').hide();

				$this.parent().find('div.alert-message.info').show()
					.find('strong')
					.css({'position':'relative', 'top':'-7px', 'left':'5px'})
					.html('Verificando e-mail...');

				$.get(
					aura.path.root + '/signup/email-verify/' + email.val(),
					function (data) {
						var status = false;

						if (data.status) {
							$this.parent().find('div.alert-message.error ul li').each(function(){
								if (!$(this).hasClass('hide'))
									status = true;
							});

							if (!status)
								$this.parent().find('div.alert-message.error').hide();

							$this.parent().find('div.alert-message.error li#email-verify').hide();
							email.addClass('success');
						}
						else {
							$this.parent().find('div.alert-message.error').show().find('li#email-verify').show();
							email.addClass('error').removeClass('success');
						}
						
						$this.parent().find('div.alert-message.info').hide().find('strong').html('');
					}, 'json'
				)
			}
			else if (!email.val() || !email_reg.test(email.val())) {
				error = true;
				email.addClass('error');
				$this.parent().find('div.alert-message.error ul li#email').show();
				$this.parent().find('div.alert-message.error').show();
			}
		});
	},

	login: function() {
		$('#facebook-login').click(function(e){
			e.preventDefault();

			FB.login(function(response) {
				if (response.authResponse) {
					location.href = aura.path.root + '/signup/facebook';
				}
			}, {scope: 'email, user_birthday, user_location, publish_stream'});
		});

		$('#login').bind('shown', function () {
			$('input[name=login-email]').focus();
		});

		$('form[name=login]').submit(function(){
			var $this = $(this);

			if (!Site.validate($this, 'warning', !$this.find('input[name=login-email]').val() || !$this.find('input[name=login-password]').val()))
				return false;

			$.post(
				$this.attr('action'),
				$this.serialize(),
				function(result) {
					if (Site.validate($this, 'error', result)) {
						location.reload();
					}
				}
			);

			return false;
		});
	},

	feedback: function() {
		$('#feedback').bind('hidden', function () {
			$('form[name=feedback]').find('div.alert-message').hide();
		});

		$('form[name=feedback]').submit(function(){
			var $this = $(this);
			var comment = $this.find('textarea[name=comment]').val();

			if ( !comment ) {
				$(':submit', this).attr('disabled', false).val('Enviar');
				$this.find('div.alert-message').removeClass('success').addClass('warning').html('Ops! Que tal nos dizer algo?').fadeIn('fast');
				return false;
			}

			$.post(
				$this.attr('action'),
				$this.serialize(),
				function(result) {
					if ( result.status !== false ) {
						$('textarea', $this).val('');
						$(':submit', $this).attr('disabled', false).val('Enviar');
						$this.find('div.alert-message').removeClass('warning').addClass('success').html('<strong>Obrigado</strong> por sua opinião, em breve sua mensagem será analisada.').fadeIn('fast');
					}
				}
			);

			return false;
		});
	},

	make_login: function() {
		$("a[rel='make-login']").click(function(e){
			e.preventDefault();
			
			$('#confirmation').modal('hide');
			$('a[rel=login]').click();
		});
	},

	forgot_password: function() {
		$('a#forgot-password').unbind('click');
		$('a#forgot-password').bind('click', function(e){
			e.preventDefault();
			var a = $(this);
			a.text('Cancelar');

			$('div#login div.alert-message.warning, div#login div.alert-message.error').hide();
			$('p#lgnp').fadeOut('fast', function(){
				$('p#fpp').fadeIn('fast');
				$('div#login input[type=submit]').attr('disabled', 'disabled');
				$('div#login a#go-register').attr('disabled', 'disabled').click(function(e){
					e.preventDefault();
				});

				a.unbind('click');
				a.bind('click', function(e){
					e.preventDefault();
					a.text('Esqueceu sua senha?');

					$('p#fpp').fadeOut('fast', function(){
						$('p#lgnp').fadeIn('fast');
						$('div#login div.alert-message.error').hide();
					});
					$('div#login input[type=submit]').removeAttr('disabled');
					$('div#login a#go-register').removeAttr('disabled');

					Site.forgot_password();
				});
			});
		});

		$("a[rel='send-mail']").unbind('click');
		$("a[rel='send-mail']").bind('click', function(e){
			e.preventDefault();

			var email = $("input[name='email-forgot-password']").val();
			var a = $(this);
			$('div#login div.modal-footer').append('<img class=\'fr\' src=\'' + aura.web.img + '/ico/ajax-loader.gif\' />');
			a.attr('disabled', 'disabled');

			$.post(
				aura.path.root + '/signup/forgot-password',
				'email=' + email,
				function(result) {
					if (result.status) {
						$.get(
							aura.path.root + '/signup/inivar/SUCCESS_MESSAGE_SEND_MAIL_FORGOT_PASSWORD',
							function(message){
								$('div#login div.alert-message.error').fadeOut('fast', function(){
									$('div#login div.alert-message.success p').html(message);
									$('div#login div.alert-message.success').fadeIn('fast');
									$('div#login div.modal-footer img').remove();
									$('div#login div.modal-footer').slideUp('fast');

									$('#login').on('hidden', function() {
										location.reload();
									});
								});
							}
						);
					}
					else {
						$.get(
							aura.path.root + '/signup/inivar/ERROR_MESSAGE_SEND_MAIL_FORGOT_PASSWORD',
							function(message){
								$('div#login div.alert-message.success').fadeOut('fast', function(){
									$('div#login div.alert-message.error p').html(message);
									$('div#login div.alert-message.error').fadeIn('fast');
									$('div#login div.modal-footer img').remove();
									a.removeAttr('disabled');
								});
							}
						);
					}
				}, 'json'
			);
		});
	},

	vote: function() {
		$('form[name=vote]').submit(function(){
			var $this = $(this);

			if (!Site.validate($this, 'warning', !$this.find('input[name=score]').val()))
				return false;

			$.post(
				$this.attr('action'),
				$this.serialize(),
				function(result) {
					if (Site.validate($this, 'error', result)) {
						$('#vote').modal('hide');
						$('a.over-voted')
							.removeAttr('data-controls-modal')
							.removeClass('pointer')
							.attr('title', 'Sua nota foi ' + ($this.find('input[name=score]').val() * 2))
							.mouseover();

						$('span.check').html('');

					}
				}
			);

			return false;
		});
	},

	basic_info: function() {
		$("form[name='basic-info']").submit(function(){
			var error = false;
			var password = $(this).find('input[name=password]');
			var confirm_password = $(this).find('input[name=confirm-password]');
			var password_span = $(this).find('input[name=password]').next('span.help-inline');
			var confirm_password_span = $(this).find("input[name='confirm-password']").next('span.help-inline');
			
			if (password.val() && password.val().length > 1) {
				if (password.val().length < 6) {
					error = true;
					password.addClass('error');
					password_span.show();
				}
				else {
					password.removeClass('error');
					password_span.hide();
				}

				if (password.val() != confirm_password.val()) {
					error = true;
					confirm_password.addClass('error');
					confirm_password_span.show();
				}
				else {
					confirm_password.removeClass('error');
					confirm_password_span.hide();
				}

				if (error) {
					$(this).find('input[type=submit]').removeAttr('disabled').val('Salvar alterações');
					return false;
				}
			}
		});
	},

	last_checkins: function() {
		$('table#last-checkins thead tr th a[rel=load]').unbind('click');
		$('table#last-checkins thead tr th a[rel=load]').bind('click', function(e){
			e.preventDefault();
			var th = $(this).parent();
			var a = th.html();

			th.html('<img src=\'' + aura.web.img + '/ico/ajax-loader-white.gif\' />');

			if ( Vars.checkins ) {
				Site.show_checkins(Vars.checkins, th, a);
			}
			else {
				$.get( aura.path.root + '/profile/last_checkins', function(data){
					Vars.checkins = data;
					Site.show_checkins(Vars.checkins, th, a);
				}, 'json');
			}
		});
	},

	show_checkins: function(data, th, a) {
		$('table#last-checkins').css('border-color', '#DDDDDD');
		var list = '';

		$.each(data, function(i, val){
			if(val['shout'] == undefined)
				var shout = '';
			else
				var shout = ' - ' + val['shout'];

			list += '<tr><td>';
			list += val['venue']['name'] + '<small class=\'info-normal\'> ' + shout + '</small>';
			list += '<input type=\'hidden\' disabled=\'disabled\' name=\'foursquare_checkin_id\' value=\'' + val['venue']['id'] + '\' />';
			list += '<input type=\'hidden\' disabled=\'disabled\' name=\'foursquare_checkin_place\' value=\'' + val['venue']['name'] + '\' />';
			list += '</td></tr>';
		});

		if (list) {
			$('table#last-checkins tbody').html(list);
			$('table#last-checkins').parent().css('height', '150px');
			th.html('Últimos checkins do Foursquare');

			$('table#last-checkins tbody').fadeIn('fast', function(){
				$(this).find('tr').click(function(){
					var remove = "&nbsp;&nbsp;<span class='icon-remove' id='remove-place'></span>";
					var theater = $(this).find('td small').remove();
						theater = $(this).find('td').html();
					
					$('div#vote').find('p small#location').html('Em <strong>' + theater + '</strong>' + remove);
					$('div#vote').find('p small#location input').removeAttr('disabled');
					$(this).parent().fadeOut('fast', function(){
						$('table#last-checkins').parent().css('height', 'auto');
						$('table#last-checkins').css('border-color', '#FFFFFF');
						th.html(a);
						Site.last_checkins();
						Site.remove_checkin();
					});
				});
			});
		}
		else
			th.html('Nenhum checkin encontrado no Foursquare');
	},

	remove_checkin: function() {
		$('span#remove-place').unbind('click');
		$('span#remove-place').bind('click', function(){
			$(this).parent().html('');
		});
	},

	person_actions: function() {
		var isVisible = false;
		var clickedAway = false;

		$("a[rel=popover]")
                .popover({
                  offset: 5,
                  placement: 'below',
                  trigger: 'manual',
                  html: true,
                  inner_class: 'minimalist'
                }).click(function(e) {
			        $(this).popover('show');
			        isVisible = true;

			        $(this).addClass('current-twipsy');
			        Site.favorite();

			        e.preventDefault();
			    });

		$(document).click(function(e) {
		  if(isVisible & clickedAway) {

			$("a[rel=popover]").each(function() {
				$(this).popover('hide');
			});

		    isVisible = clickedAway = false;

		  }
		  else
		    clickedAway = true;
		});
	},

	next_movie: function() {
		$('a[rel=next-movie]').unbind('click');
		$('a[rel=next-movie]').bind('click', function(e){
			e.preventDefault();
			var $this = $(this);
			var id = $this.attr('data-item-id');
			var action = $this.attr('data-item-action');

			if ( id ) {
				$.post(
					aura.path.root + '/profile/next-movie',
					'action=' + action + '&id=' + id,
					function( response ) {
						if ( response !== true )
							$this.parent().text('Ops! Algo deu errado, tente novamente :(');
						else {
							if ( action == 'check' ) {
								$this.text('Marcado como próximo');
								$this.attr('data-item-action', 'uncheck');
								$("<span class='icon-check'></span>").insertBefore($this);
							}
							else {
								$this.text('Marcar como próximo');
								$this.attr('data-item-action', 'check');
								$this.prev('span').remove();
							}
						}
					}, 'json'
				)
			}
		});
	},

	favorite: function( only_remove ) {
		$('a[rel=favorite]').unbind('click');
		$('a[rel=favorite]').bind('click', function(e){
			e.preventDefault();
			var $this = $(this);
			var id = $this.attr('data-item-id');
			var type = $this.attr('data-item-type');
			var action = $this.attr('data-item-action');

			if ( type && id ) {
				if ( !only_remove )
					$this.text('(Des)Favoritar');

				$.post(
					aura.path.root + '/profile/favorite',
					'type=' + type + '&action=' + action + '&id=' + id,
					function( response ) {
						if ( only_remove )
							Site.remove_from_profile( $this );
						else {
							if ( type == 'movie' )
								Site.favorite_movie( response, action, $this );
							else
								Site.favorite_others( response, action, $this, id );
						}
					}, 'json'
				)
			}
		});
	},

	favorite_others: function( response, action, $this, id ) {
		if ( response !== true )
			$this.parent().text('Ops! Algo deu errado, tente novamente :(');
		else {
			var html_data = $("a[rel=popover][data-item-id=" + id + "]").attr('data-content');

			if ( action == 'favorite' ) {
				$this.text('(Des)Favoritar');
				$this.attr('data-item-action', 'unfavorite');
				$this.prev('span').attr('class', 'icon-star');

				html_data = html_data.replace("data-item-action='favorite'", "data-item-action='unfavorite'");
				html_data = html_data.replace("Favoritar", "(Des)Favoritar");
				html_data = html_data.replace("icon-star-empty", "icon-star");

				$("<span class='icon-star'></span>").insertBefore($("a[rel=popover][data-item-id=" + id + "]"));
			}
			else {
				$this.text('Favoritar');
				$this.attr('data-item-action', 'favorite');
				$this.prev('span').attr('class', 'icon-star-empty');

				html_data = html_data.replace("data-item-action='unfavorite'", "data-item-action='favorite'");
				html_data = html_data.replace("(Des)Favoritar", "Favoritar");
				html_data = html_data.replace("icon-star", "icon-star-empty");

				$("a[rel=popover][data-item-id=" + id + "]").prev('span').remove();
			}

			$("a[rel=popover][data-item-id=" + id + "]").attr('data-content', html_data);
		}
	},

	favorite_movie: function( response, action, $this ) {
		if ( response !== true )
			$this.parent().text('Ops! Algo deu errado, tente novamente :(');
		else {
			if ( action == 'favorite' ) {
				$this.attr('data-item-action', 'unfavorite');
				$this.prev('span').attr('class', 'icon-star');
			}
			else {
				$this.text('Favoritar');
				$this.attr('data-item-action', 'favorite');
				$this.prev('span').attr('class', 'icon-star-empty');
			}
		}
	},

	remove_from_profile: function( $this ) {
		$this.parent().fadeOut('fast', function() { $(this).remove(); });
	},

	slide_poster_load: function() {
		Site.image_loader( '#slideshow li', '#slideshow li img:eq(0)' );
	},

	home_thumbs_load: function() {
		$('div.premieres ul li').each(function(){
			Site.image_loader( $(this).find('a'), $(this).find('a img'), 'white' );
		});

		$('dl.movie-box').each(function(){
			Site.image_loader( $(this).find('dd span a'), $(this).find('dd span a img'), 'white' );
		})
	},

	image_loader: function( container, img, loader ) {
		loader = loader ? 'ajax-loader-' + loader + '.gif' : 'ajax-loader.gif';
		$(container)
			.css({
				'background': 'url(' + aura.web.img + '/ico/' + loader + ') no-repeat center center'
			});

		$(img).load(function(){
			$(container).css('background-image', 'none');
		});
	},

	analytics: {
		track_event: function( element, category, action, location ) {
			var time = 100;
			location = location ? location : true;

			switch ( $(element).get(0).tagName.toLowerCase() )
			{
				case 'a':
					if ( typeof _gaq == 'undefined' ) {
						if ( location )
							document.location = $(element).attr('href');

						return false;
					}

					_gaq.push(['_trackEvent', category, action]);

					if ( location )
						setTimeout( 'document.location = \'' + $(element).attr('href') + '\'', time );
				break;

				case 'input':
					if ( $(element).attr('type') == 'submit' ) {
							if ( typeof _gaq == 'undefined' ) {
								$(element).parents('form').submit();
								return false;
							}

							_gaq.push(['_trackEvent', category, action]);
							setTimeout( function(){ $(element).parents('form').submit() }, time );
						}
				break;
			}
		}
	}
};