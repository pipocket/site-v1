<?php
    class MovieModel extends AppModel
	{
		protected $table_name = "movie";
		protected $table_key = "id";
		protected $table_exception = "status = 1";

		public function ranking()
		{
			$data = $this->getAll( array('where' => $this->rules( 'ranking' ), 'limit' => 3, 'order' => 'rate desc') );

			foreach ( $data['data'] as $key => $movie ) {
				$movie->thumb = $this->MovieThumb->by_movie( $movie->id, 'thumb1' );
				$movie->banner = $this->MoviePoster->by_movie( $movie->id );

				if ( $key == 0 )
					$movie->first = true;
				else if ( $key ==2 )
					$movie->last = false;
			}

			return $data['data'];
		}

		public function release( $external_limit = null, $thumb = false, $gender = false, $text_search = null )
		{
			$additional_where = null;
			$limit = 3;
			$order = 'asc';

			if ( $this->security() )
				$limit = 8;

			if ( $external_limit )
				$limit = $external_limit;

			if ( $text_search )
				$additional_where = " AND ( name LIKE '%$text_search%' OR original_name LIKE '%$text_search%' )";

			$data = $this->getAll( array('limit' => $limit, 'order' => '`release` ' . $order, 'where' => $this->rules( 'release' ) . $additional_where ) );

			foreach ($data['data'] as $movie) {
				if ( $this->security() OR $thumb )
					$movie->thumb = $this->MovieThumb->by_movie( $movie->id, 'thumb2' );
				else
					$movie->thumb = $this->MovieThumb->by_movie( $movie->id, 'poster' );

				if ( $gender )
					$movie->gender = $this->MovieGender->by_movie( $movie->id );

				if ( date('Y-m-d', strtotime( $movie->release )) == date('Y-m-d') )
					$movie->release = 'Hoje';
				else
					$movie->release = date('d/m/Y', strtotime( $movie->release ) );

				$movie->directors = $this->MovieDirector->by_movie( $movie->id );
			}

			return $data['data'];
		}

		public function one( $key, $comment_short = true, $user_id = null )
		{
			$movie = $this->getByKey( $key );
			$movie = isset($movie['data'][0]) ? $movie['data'][0] : false;

			if ( $movie ) {
				$movie->genders = $this->MovieGender->by_movie( $movie->id );
				$movie->directors = $this->MovieDirector->by_movie( $movie->id );
				$movie->actors = $this->MovieActor->by_movie( $movie->id );
				$movie->poster = $this->MoviePoster->by_movie( $movie->id );
				$movie->thumb_facebook = $this->MovieThumb->by_movie( $movie->id, 'facebook' );
				$movie->thumb = $this->MovieThumb->by_movie( $movie->id, 'thumb2' );
				$movie->thumb2 = $this->MovieThumb->by_movie( $movie->id, 'poster' );
				$movie->comments = $this->Rate->comments_by( 'movie', 'user', $movie->id, $comment_short );
				$movie->voted = $this->Rate->voted( $movie->id, $user_id );

				// Open to vote
				$release_date_parse = explode( '-' , date( 'Y-m-d', strtotime( $movie->release ) ) );
				$movie->open = date( 'Y-m-d', mktime( '00', '00', '00', $release_date_parse[1], $release_date_parse[2]-10, $release_date_parse[0] ) );
				$movie->open = ( $movie->open <= date( 'Y-m-d' ) );

				$movie->release_date = Helper::date_format( 'd/m', $movie->release );
				$movie->release = ( date('Y-m-d', strtotime( $movie->release ) ) <= date( 'Y-m-d' ) );
				$movie->next = $this->UserNextMovie->by_movie( $movie->id );
				$movie->viewed = $this->UserMovie->viewed( $movie->id );
				$movie->favorite = $this->UserMovie->favorite_by_movie( $movie->id );
				$movie->user_preferences = $this->UserPreference->by_user();
				$this->session( 'movie', base64_encode( $movie->id ) );
			}

			return $movie;
		}

		public function data( $id, $fields )
		{
			$sql = $this->database()->prepare( "SELECT $fields FROM {$this->table_name} WHERE id = ?" );
			$sql->execute( array( $id ) );
			$result = $sql->fetch( PDO::FETCH_OBJ );

			return $result;
		}

		public function update_rate( $movie_id, $value )
		{
			return $this->set( array('data' => array( 'id' => $movie_id, 'rate' => $value ) ) );
		}

		public function search( $title, $cast, $director, $gender, $type, $limit = null )
		{
			if ( !$title && !$cast && !$director && !$gender && !$type )
				return null;

			$where = null;
			$orderby = null;
			$actors = null;
			$directors = null;
			$genders = null;

			if ( $title )
				$where .= "(name LIKE '%$title%' OR original_name LIKE '%$title%')";

			if ( $cast ) {
				$cast = $this->Actor->by_name( $cast );

				foreach ( $cast as $c )
					$actors .= "actor_id = {$c->id} OR ";

				if ( $actors ) {
					$actors = substr( $actors, 0, -3 );
					$where .= $actors ? $where ? "AND ($actors)" : "($actors)" : null;
				}
			}

			if ( $director ) {
				$director = $this->Director->by_name( $director );

				foreach ( $director as $d )
					$directors .= "director_id = {$d->id} OR ";

				if ( $directors ) {
					$directors = substr( $directors, 0, -3 );
					$where .= $directors ? $where ? "AND ($directors)" : "($directors)" : null;
				}
			}

			if ( $gender ) {
				$gender = $this->Gender->by_description( $gender );

				foreach ( $gender as $g )
					$genders .= "gender_id = {$g->id} OR ";

				if ( $genders ) {
					$genders = substr( $genders, 0, -3 );
					$where .= $genders ? $where ? "AND ($genders)" : "($genders)" : null;
				}
			}

			if ( $type ) {

				switch ( $type ) {
					case 'lideres-de-audiencia':
						$clause = $this->rules( 'ranking' );
						$where .= $where ? " AND $clause " : $clause;
						$orderby = 'rate desc';
						break;

					case 'estreias':
						$clause = $this->rules( 'release' );;
						$where .= $where ? " AND $clause " : $clause;
						$orderby = '`release`';
						break;

					case 'em-cartaz':
						$clause = $this->rules( 'in-theaters' );
						$where .= $where ? " AND $clause " : $clause;
						$orderby = 'rate desc';
						break;

					case 'arquivados':
						$clause = $this->rules( 'filed' );
						$where .= $where ? " AND $clause " : $clause;
						$orderby = 'rate desc';
						break;

					case 'todos':
						$orderby = 'name';
						break;
				}

			}

			if ( $limit )
				$limit = "Limit $limit";

			if ( $where )
				$where = 'WHERE ' . $where;
			elseif ( !$orderby )
				return null;

			if ( $orderby )
				$orderby = 'ORDER BY ' . $orderby;

			$sql = $this->database()->prepare( "SELECT DISTINCT movie.id, movie.name, movie.original_name, movie.`key`, movie.poster as thumb, movie.thumb2, movie.rate, movie.preview, movie.`release`, movie.`time`, date_add(date(`release`), INTERVAL 45 DAY) as release_interval, null as current FROM movie_search movie $where $orderby $limit" );
			$sql->execute();

			$movies = $sql->fetchAll( PDO::FETCH_OBJ );

			if ( $type == 'todos' ) {
				foreach ( $movies as $movie ) {
					$preview = date('Y-m-d', strtotime( $movie->preview ) );
					$release = date('Y-m-d', strtotime( $movie->release ) );
					$release_interval = date('Y-m-d', strtotime( $movie->release_interval ) );

					if ( $preview == date( 'Y-m-d' ) )
						$movie->current = 'pré-estréia';
					elseif ( $release == date( 'Y-m-d' ) )
						$movie->current = 'estréia';
					elseif ( $release > date( 'Y-m-d' ) )
						$movie->current = join( '/', array_reverse( explode( '-', $release ) ) );
					elseif ( $release == date( 'Y-m-d' ) )
						$movie->current = 'estréia';
					elseif ( $release_interval >= date( 'Y-m-d' ) )
						$movie->current = 'em cartaz';
					elseif ( $release_interval < date( 'Y-m-d' ) )
						$movie->current = 'arquivado';
					else
						$movie->current = '';
				}
			}

			return $movies;
		}

		// Em cartaz
		public function in_theaters()
		{
			$sql = $this->database()->prepare( "SELECT id FROM {$this->table_name} WHERE {$this->rules('in-theaters')}" );
			$sql->execute();

			return $sql->fetchAll( PDO::FETCH_OBJ );
		}

		public function ranking_interval()
		{
			$sql = $this->database()->prepare( "SELECT id FROM {$this->table_name} WHERE {$this->rules('ranking-interval')}" );
			$sql->execute();

			return $sql->fetchAll( PDO::FETCH_OBJ );
		}

		public function ranking_movie()
		{
			$sql = $this->database()->prepare( "SELECT id FROM {$this->table_name} WHERE {$this->rules('ranking-interval')} AND rate > 0" );
			$sql->execute();

			return $sql->fetchAll( PDO::FETCH_OBJ );
		}

		public function rules( $which )
		{
			switch ( $which ) {
				case 'in-theaters':
					return "`release` <= now() AND date_add(date(`release`), INTERVAL 45 DAY) >= curdate()";
					break;

				case 'release':
					return "date_format(`release`, '%Y%m%d') >= curdate()";
					break;

				case 'ranking':
					$minimum = $this->Rate->minimum_common();
					$condition = " AND (SELECT COUNT(*) FROM rate WHERE movie_id = movie.id) >= {$minimum}";

					return "rate > 0 AND `release` <= now() AND date_add(date(`release`), INTERVAL 90 DAY) >= curdate() {$condition}";
					break;

				case 'ranking-interval':
					return "`release` <= now() AND date_add(date(`release`), INTERVAL 90 DAY) >= curdate()";
					break;

				case 'filed':
					return "`release` <= now() AND date_add(date(`release`), INTERVAL 45 DAY) < curdate()";
					break;

				default:
					return null;
					break;
			}
		}
	}
?>