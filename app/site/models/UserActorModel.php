<?php
    class UserActorModel extends AppModel
	{
		protected $table_name = "user_actor";
		protected $table_key = "";
		protected $table_related = "actor";
		
		public function by_user( $user_id )
		{
			return $this->related( 'user_id', $user_id, 'name', true );
		}

		public function ids_by_user()
		{
			$sql = $this->database()->prepare( "SELECT actor_id as id FROM {$this->table_name} WHERE user_id = ?" );
			$sql->execute( array( $this->userid() ) );

			return $sql->fetchAll( PDO::FETCH_OBJ );
		}

		public function add( $actor_id )
		{
			try {
				$sql = $this->database()->prepare( "INSERT INTO {$this->table_name} (user_id, actor_id) VALUES (?,?)" );
				return $sql->execute( array( $this->userid(), $actor_id ) );
			}
			catch( exception $e ) {
				return false;
			}
		}

		public function remove( $actor_id )
		{
			try {
				$sql = $this->database()->prepare( "DELETE FROM {$this->table_name} WHERE user_id = ? AND actor_id = ?" );
				return $sql->execute( array( $this->userid(), $actor_id ) );
			}
			catch( exception $e ) {
				return false;
			}
		}
	}
?>