<?php
    class UserMovieModel extends AppModel
	{
		protected $table_name = "user_movie";
		protected $table_key = "";
		protected $table_related = "movie";
		
		public function by_user( $user_id )
		{
			return $this->related_wlink( 'user_id', $user_id, array('name', 'key') );
		}

		public function favorite_by_user( $user_id )
		{
			$sql = $this->database()->prepare( "SELECT um.*, m.id, m.name, m.key, mt.thumb2 as thumb FROM {$this->table_name} um JOIN movie m ON m.id = um.movie_id LEFT JOIN movie_thumb mt ON mt.movie_id = m.id WHERE um.user_id = ? AND um.favorite = 1" );
			$sql->execute( array( $user_id ) );
			
			$favorites = $sql->fetchAll( PDO::FETCH_OBJ );

			return $favorites;
		}

		public function favorite_by_movie( $movie_id )
		{
			$sql = $this->database()->prepare( "SELECT COUNT(*) as n FROM {$this->table_name} WHERE user_id = ? AND movie_id = ? AND favorite = 1" );
			$sql->execute( array( $this->userid(), $movie_id ) );
			
			return $sql->fetch( PDO::FETCH_OBJ )->n == 1 ? true : false;
		}

		public function viewed( $movie_id )
		{
			$sql = $this->database()->prepare( "SELECT COUNT(*) as n FROM {$this->table_name} WHERE user_id = ? AND movie_id = ?" );
			$sql->execute( array( $this->userid(), $movie_id ) );
			
			return $sql->fetch( PDO::FETCH_OBJ )->n == 1 ? true : false;
		}

		public function add( $movie_id )
		{
			try {
				$sql = $this->database()->prepare( "INSERT INTO {$this->table_name} (user_id, movie_id) VALUES (?,?)" );
				return $sql->execute( array( $this->userid(), $movie_id ) );
			}
			catch( exception $e ) {
				return false;
			}
		}

		public function remove( $movie_id )
		{
			try {
				$sql = $this->database()->prepare( "DELETE FROM {$this->table_name} WHERE user_id = ? AND movie_id = ?" );
				return $sql->execute( array( $this->userid(), $movie_id ) );
			}
			catch( exception $e ) {
				return false;
			}
		}

		public function favorite( $movie_id )
		{
			try {
				$sql = $this->database()->prepare( "UPDATE {$this->table_name} SET favorite = 1 WHERE user_id = ? AND movie_id = ?" );
				return $sql->execute( array( $this->userid(), $movie_id ) );
			}
			catch( exception $e ) {
				return false;
			}
		}

		public function unfavorite( $movie_id )
		{
			try {
				$sql = $this->database()->prepare( "UPDATE {$this->table_name} SET favorite = 0 WHERE user_id = ? AND movie_id = ?" );
				return $sql->execute( array( $this->userid(), $movie_id ) );
			}
			catch( exception $e ) {
				return false;
			}
		}
	}
?>