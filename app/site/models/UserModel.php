<?php
    class UserModel extends AppModel
	{
		protected $table_name = "user";
		protected $table_key = "id";
		protected $data_status;
		public $post_name;
		public $post_email;
		protected $post_password;
		protected $facebook_id;
		protected $twitter_id;
		protected $birthdate;
		protected $location;
		protected $mode = "default";

		public function add( $social_plugin = false, $twitter_post = false )
		{
			if ( !$social_plugin )
				$this->check_data_register();

			if ( ( isset( $this->data_status->status ) AND $this->data_status->status ) OR $this->data_status === TRUE ) {
				$status_of_register = $social_plugin ? 1 : 0;
				$database = $this->database();

				$sql = $database->prepare( "SELECT COUNT(*) as n FROM {$this->table_name} WHERE email = ?" );
				$sql->execute( array( $this->post_email ) );
				
				if ( $sql->fetch( PDO::FETCH_OBJ )->n > 0 )
					return (object)array( 'status' => false, 'data' => array('register_validate_user_exists' => true) );

				$sql = $database->prepare( "INSERT INTO {$this->table_name} (name, email, birthdate, location, password, facebook, twitter, status) VALUES (?,?,?,?,?,?,?,?)" );
				$result = $sql->execute( array( $this->post_name, $this->post_email, $this->birthdate, $this->location, Helper::md5( $this->post_password ), $this->facebook_id, $this->twitter_id, $status_of_register ) );

				if ( $result ) {
					$this->message( 'register', true );
					$this->UserPreference->add( $database->lastInsertId(), $twitter_post, $this->facebook_id );

					return $this->data_status;
				}

				return (object)array( 'status' => false );
			}

			return $this->data_status;
		}

		private function check_data_register( $password_only = false )
		{
			$validate = true;
			$error_data = array();
			
			if ( !$password_only ) {
				$this->post_name = isset( $_POST['name'] ) ? $_POST['name'] : null;
				$this->post_email = isset( $_POST['email'] ) ? $_POST['email'] : null;
			}

			$this->post_password = isset( $_POST['password'] ) ? $_POST['password'] : null;
			$confirm_password = isset( $_POST['confirm-password'] ) ? $_POST['confirm-password'] : null;

			if ( !$password_only ) {
				if ( count( explode( ' ', $this->post_name ) ) == 1 ) {
					$error_data['register_validate_name'] = true ;
					$validate = false;
				}
				if ( !Helper::email_validate( $this->post_email ) ) {
					$error_data['register_validate_email'] = true ;
					$validate = false;
				}
			}

			if ( ( !$this->post_password OR !$confirm_password ) OR ( $this->post_password !== $confirm_password ) ) {
				$error_data['register_validate_password'] = true;
				$validate = false;
			}
			elseif ( strlen( $this->post_password ) < 6 ) {
				$error_data['register_validate_maxlength_password'] = true;
				$validate = false;
			}

			$this->data_status = (object)array( 'status' => $validate, 'data' => $error_data );
		}

		public function one( $id )
		{
			$id = $id ? $id : base64_decode( $this->session( 'userid' ) );
			$user = $this->getAll( array( 'where' => "id = $id", 'limit' => 1 ) );
			$user = isset( $user['data'][0] ) ? $user['data'][0] : null;
			
			if ( $user ) {
				$user->profile = $user->id == base64_decode( $this->session( 'userid' ) ) ? true : false;
				$user->genders = $this->UserGender->by_user( $user->id );
				$user->actors = $this->UserActor->by_user( $user->id );
				$user->directors = $this->UserDirector->by_user( $user->id );
				$user->comments = $this->Rate->comments_by( 'user', 'movie', $user->id );
				$user->movies = $this->UserMovie->by_user( $user->id );
				$user->next_movies = $this->UserNextMovie->by_user( $user->id );
				$favorites = $this->UserMovie->favorite_by_user( $user->id );

				if ( count($favorites) >= 6 ) {
					$user->movies_sidebar = array_slice( $favorites, 0, 6 );
					$user->movies_favorite = array_slice( $favorites, 6, count( $favorites ) - 1 );
				}
				else {
					$user->movies_sidebar = array();
					$user->movies_favorite = $favorites;
				}

				return $user;
			}
			else
				return false;
		}
		
		public function facebook()
		{
			$data = $this->facebook_data();
			
			if ( !isset( $data['error'] ) ) {
				$this->mode = 'facebook';
				$this->facebook_id = $data['id'];
				$this->post_name = $data['name'];
				$this->birthdate = $data['birthday'];
				$this->location = isset( $data['location']['name'] ) ? $data['location']['name'] : null;

				if ( !$this->facebook_verify( $this->facebook_id ) && !$this->email_verify( $data['email'] ) ) {
					$this->data_status = true;
					$this->post_email = $data['email'];
					$this->post_password = 'facebook';
					
					$result = $this->add( true );
					
					if( $result ) {
						$_SERVER['REQUEST_METHOD'] = 'POST';
						$_POST['login-email'] = $this->post_email;
						$_POST['login-password'] = $this->post_password;

						return $this->login();
					}
				}
				else {
					$_SERVER['REQUEST_METHOD'] = 'POST';
					$_POST['login-email'] = $data['email'];
					$_POST['name'] = $data['name'];

					return $this->login( true );
				}
			}
			else
				return false;
		}

		public function twitter()
		{
			/* Get user access tokens out of the session. */
			$access_token = $_SESSION['access_token'];

			/* Create a TwitterOauth object with consumer/user tokens. */
			$params = array( TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret'] );
			$connection = $this->vendors( 'TwitterOAuth', $params );

			/* If method is set change API call made. Test is called by default. */
			$content = $connection->get( 'account/verify_credentials' );

			if ( isset( $content->error ) && $content->error == 'Could not authenticate with OAuth.' ) {
				$this->User->twitter_connect();
				exit;
			}

			$this->mode = 'twitter';
			$this->twitter_id = $content->id_str;
			$this->post_name = $content->name;
			$this->location = isset( $content->location ) ? $content->location : null;

			if ( !$this->twitter_verify() ) {
				$this->data_status = true;
				$this->post_password = 'twitter';

				$login = $this->add( true, true );
			}
			else
				$login = true;
			
			if( $login ) {
				$_SERVER['REQUEST_METHOD'] = 'POST';
				return $this->login( true, true );
			}
			else
				return false;
		}

		public function twitter_clear()
		{
			$this->session( 'oauth_token', false, true );
			$this->session( 'oauth_token_secret', false, true );
			$this->session( 'access_token', false, true );
			$this->session( 'oauth_status', false, true );
			$this->session( 'status', false, true );
		}

		public function twitter_connect( $action = null )
		{
			$this->twitter_clear();

			/* Build TwitterOAuth object with client credentials. */
			$params = array( TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET );
			$connection = $this->vendors( 'TwitterOAuth', $params );
			 
			/* Get temporary credentials. */
			$request_token = $connection->getRequestToken( TWITTER_OAUTH_CALLBACK . $action );

			/* Save temporary credentials to session. */
			$this->session( 'oauth_token', $request_token['oauth_token'] );
			$this->session( 'oauth_token_secret', $request_token['oauth_token_secret'] );
			$token = $request_token['oauth_token'];

			/* If last connection failed don't display authorization link. */
			switch ( $connection->http_code ) {
			  case 200:
			    /* Build authorize URL and redirect user to Twitter. */
			    $url = $connection->getAuthorizeURL( $token );
			    $this->redirect( $url, null, true );
			    break;
			  default:
			    /* Show notification if something went wrong. */
			    echo 'Could not connect to Twitter. Refresh the page or try again later.';
			}
		}

		public function twitter_callback( $url_redirect = null )
		{
			if( strrpos( $_SERVER['REQUEST_URI'], '?denied=' ) === false ) {
				$request = end( explode( '?oauth_token=', $_SERVER['REQUEST_URI'] ) );
				$request = explode( '&oauth_verifier=', $request );
				$_REQUEST['oauth_token'] = reset( $request );
				$_REQUEST['oauth_verifier'] = end( $request );

				/* If the oauth_token is old redirect to the connect page. */
				if ( isset( $_REQUEST['oauth_token'] ) && $this->session( 'oauth_token' ) !== $_REQUEST['oauth_token'] ) {
				  $this->session( 'oauth_status', 'oldtoken' );
				  $this->redirect( '/signup/twitter/connect' );
				}
				
				/* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
				$params = array( TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, $this->session( 'oauth_token' ), $this->session( 'oauth_token_secret' ) );
				$connection = $this->vendors( 'TwitterOAuth', $params );

				/* Request access tokens from twitter */
				$access_token = $connection->getAccessToken( $_REQUEST['oauth_verifier'] );

				/* Save the access tokens. Normally these would be saved in a database for future use. */
				$_SESSION['access_token'] = $access_token;

				/* Remove no longer needed request tokens */
				$this->session( 'oauth_token', false, true );
				$this->session( 'oauth_token_secret', false, true );

				/* If HTTP response is 200 continue otherwise send to connect page to retry */
				if ( 200 == $connection->http_code ) {
				  $url_success = $url_redirect ? $url_redirect : '/signup/twitter';

				  /* The user has been verified and the access tokens can be saved for future use */
				  $this->session( 'status', 'verified' );
				  $this->redirect( $url_success );
				} else {
				  $url_error = $url_redirect ? $url_redirect : '/signup/twitter/connect';

				  /* Save HTTP status for error dialog on connnect page.*/
				  $this->redirect( $url_error );
				}
			}
			else
				$this->redirect( '/profile/twitter_denied' );
		}

		public function update_twitter_id()
		{
			$id = base64_decode( $this->session( 'userid' ) );

			/* Get user access tokens out of the session. */
			$access_token = $_SESSION['access_token'];

			/* Create a TwitterOauth object with consumer/user tokens. */
			$params = array( TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret'] );
			$connection = $this->vendors( 'TwitterOAuth', $params );

			/* If method is set change API call made. Test is called by default. */
			$content = $connection->get( 'account/verify_credentials' );

			if ( isset( $content->error ) && $content->error == 'Could not authenticate with OAuth.' ) {
				Helper::log( 'social_network', $content->error . ' | user_id: ' . $id );
				return false;
			}

			try {
				$sql = $this->database()->prepare( "UPDATE {$this->table_name} SET twitter = ?, twitter_oauth_token = ?, twitter_oauth_token_secret = ? WHERE id = ?" );
				return $sql->execute( array( $content->id_str, $access_token['oauth_token'], $access_token['oauth_token_secret'], $id ) );
			}
			catch( exception $e ) {
				return false;
			}
		}

		public function foursquare( $code )
		{
			if ( !$code ) {
				$this->redirect( 'https://foursquare.com/oauth2/authenticate?client_id=' . FOURSQUARE_CLIENT_ID . '&response_type=code&redirect_uri=' . FOURSQUARE_URL_CALLBACK, null, true );
			}
			else {
				$url = 'https://foursquare.com/oauth2/access_token?client_id=' . FOURSQUARE_CLIENT_ID . '&client_secret=' . FOURSQUARE_SECRET . '&grant_type=authorization_code&redirect_uri=' . FOURSQUARE_URL_CALLBACK . '&code=' . $code;

				$ch = curl_init();
				curl_setopt( $ch, CURLOPT_URL, $url );
				curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
				$data = curl_exec( $ch );
				curl_close( $ch );
				
				$data = json_decode( $data, true );
				return $data;
			}
		}

		public function update_foursquare_token( $content )
		{
			$id = base64_decode( $this->session( 'userid' ) );

			if ( isset( $content['error'] ) ) {
				Helper::log( 'social_network', $content['error'] . ' | user_id: ' . $id );
				return false;
			}

			try {
				$sql = $this->database()->prepare( "UPDATE {$this->table_name} SET foursquare = ? WHERE id = ?" );
				return $sql->execute( array( $content['access_token'], $id ) );
			}
			catch( exception $e ) {
				return false;
			}
		}

		public function login( $facebook = false, $twitter = false )
		{
			if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
				$email = isset( $_POST['login-email'] ) ? addslashes( $_POST['login-email'] ) : null;
				$password = isset( $_POST['login-password'] ) ? Helper::md5( addslashes( $_POST['login-password'] ) ) : null;

				if ( $twitter ) {
					if ( $this->twitter_id ) {
						$sql = $this->database()->prepare( "SELECT id, name, email, security FROM {$this->table_name} WHERE twitter = ?" );
						$sql->execute( array( $this->twitter_id ) );
						$user = $sql->fetch( PDO::FETCH_OBJ );
					}
				}
				elseif ( $facebook ) {
					if ( $this->facebook_id ) {
						$sql = $this->database()->prepare( "SELECT id, name, email, security FROM {$this->table_name} WHERE facebook = ?" );
						$sql->execute( array( $this->facebook_id ) );
						$user = $sql->fetch( PDO::FETCH_OBJ );

						if ( !$user && $email ) {
							$sql = $this->database()->prepare( "SELECT id, name, email, security FROM {$this->table_name} WHERE email = ?" );
							$sql->execute( array( $email ) );
							$user = $sql->fetch( PDO::FETCH_OBJ );
						}
					}
				}
				else {
					if ( $email && $password ) {
						$sql = $this->database()->prepare( "SELECT id, name, email, security FROM {$this->table_name} WHERE email = ? AND password = ? AND status = 1" );
						$sql->execute( array( $email, $password ) );
						$user = $sql->fetch( PDO::FETCH_OBJ );
					}
				}

				if ( isset( $user ) && $user )
					$result = $this->login_session( $user );

				if ( isset( $result ) && $result ) {
					$ip = $_SERVER['REMOTE_ADDR'];

					if ( $twitter ) {
						$sql = $this->database()->prepare( "UPDATE {$this->table_name} SET name = ?, location = ?, last_login = now(), last_ip = ?, last_mode = ? WHERE twitter = ?" );
						$result = $sql->execute( array( $this->post_name, $this->location, $ip, $this->mode, $this->twitter_id ) );
					}
					else {
						$facebook = $facebook ? ", facebook = '{$this->facebook_id}'" : '';
						$name = $facebook ? ", name = '{$this->post_name}'" : '';
						
						$sql = $this->database()->prepare( "UPDATE {$this->table_name} SET birthdate = ?, location = ?, last_login = now(), last_ip = ?, last_mode = ? $facebook $name WHERE email = ?" );
						$result = $sql->execute( array( $this->birthdate, $this->location, $ip, $this->mode, $email ) );
					}

					return $result;
				}
			}

			return false;
		}

		public function update()
		{
			$id = base64_decode( $this->session( 'userid' ) );
			$current_location = $this->data( $id, 'location' );
			$current_location = $current_location->location;
			$current_email = $this->data( $id, 'email' )->email;
			$gender = $_POST['gender'];

			if ( !$id OR !is_numeric( $id ) OR strlen( $id ) < 6 )
				return false;

			if ( isset( $_POST['email'] ) && !$current_email && Helper::email_validate( $_POST['email'] ) ) {
				$sql = $this->database()->prepare( "SELECT COUNT(*) as n FROM {$this->table_name} WHERE email = ?" );
				$sql->execute( array( $_POST['email'] ) );
				$result = $sql->fetch( PDO::FETCH_OBJ )->n;
				
				if ( $result > 0 ) {
					$this->message( 'register_validate_email_in_use', true );
					return false;
				}

				$email = $_POST['email'];
			}
			else
				$email = $current_email;

			$location = isset( $_POST['location'] ) && !$current_location ? $_POST['location'] : $current_location;
			$this->check_data_register( true );

			if ( $this->data_status ) {
				$sql = $this->database()->prepare( "UPDATE {$this->table_name} SET email = ?, gender = ?, location = ?, password = ? WHERE id = ?" );
				return $sql->execute( array( $email, $gender, $location, Helper::md5( $this->post_password ), $id ) );
			}
			else
				$sql = $this->database()->prepare( "UPDATE {$this->table_name} SET email = ?, gender = ?, location = ? WHERE id = ?" );
				return $sql->execute( array( $email, $gender, $location, $id ) );
		}

		public function update_facebook_id()
		{
			$id = base64_decode( $this->session( 'userid' ) );
			$user_facebook = $this->facebook_data();

			$data = array( 'data' => array( 'id' => $id, 'facebook' => $user_facebook['id'], 'birthdate' => $user_facebook['birthday'] ) );
			return $this->set( $data );
		}

		public function data( $id, $fields )
		{
			$id = $id ? $id : base64_decode( $this->session( 'userid' ) );

			$sql = $this->database()->prepare( "SELECT {$fields} FROM {$this->table_name} WHERE id = ?" );
			$sql->execute( array( $id ) );
			$result = $sql->fetch( PDO::FETCH_OBJ );

			return $result;
		}

		public function email_verify( $email, $fields = 'email' )
		{
			if ( $email ) {
				$sql = $this->database()->prepare( "SELECT $fields FROM {$this->table_name} WHERE email = ?" );
				$sql->execute( array( $email ) );
				return $sql->fetch( PDO::FETCH_OBJ );
			}
		}

		public function facebook_verify( $facebook_id, $fields = 'facebook' )
		{
			if ( $facebook_id ) {
				$sql = $this->database()->prepare( "SELECT $fields FROM {$this->table_name} WHERE facebook = ?" );
				$sql->execute( array( $facebook_id ) );
				return $sql->fetch( PDO::FETCH_OBJ );
			}
		}

		public function twitter_verify()
		{
			if ( $this->twitter_id ) {
				$sql = $this->database()->prepare( "SELECT twitter FROM {$this->table_name} WHERE twitter = ?" );
				$sql->execute( array( $this->twitter_id ) );
				return $sql->fetch( PDO::FETCH_OBJ );
			}
		}

		public function last_checkins()
		{
			$id = base64_decode( $this->session( 'userid' ) );
			$data = $this->getById( $id );

			$ch = curl_init();
			curl_setopt( $ch, CURLOPT_URL, "https://api.foursquare.com/v2/users/self/checkins?limit=10&oauth_token={$data['data']->foursquare}&v=" . date('Ymd') );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );

			$data = curl_exec( $ch );
			curl_close( $ch );
			
			return $data;
		}

		public function activation( $email )
		{
			$email = base64_decode($email);

			if ( $email && Helper::email_validate( $email ) ) {
				$sql = $this->database()->prepare( "SELECT email, status FROM {$this->table_name} WHERE email = ?" );
				$sql->execute( array( $email ) );
				$user = $sql->fetch( PDO::FETCH_OBJ );

				if ( $user ) {
					if ($user->status == 1)
						return 'already-activated';
					
					$sql = $this->database()->prepare( "UPDATE {$this->table_name} SET status = 1 WHERE email = ?" );
					$sql->execute( array( $user->email ) );
					
					return 'activated';
				}
			}

			return false;
		}

		public function change_status( $email, $status )
		{
			if ( $email ) {
				$sql = $this->database()->prepare( "UPDATE {$this->table_name} SET status = ? WHERE email = ?" );
				return $sql->execute( array( $status, $email ) );
			}
		}

		public function check_status_password( $email )
		{
			if ( $email ) {
				$email = base64_decode($email);
				$sql = $this->database()->prepare( "SELECT status FROM {$this->table_name} WHERE email = ? AND status = 2" );
				$sql->execute( array( $email ) );
				return $sql->fetch( PDO::FETCH_OBJ );
			}
		}

		public function redefine_password( $email )
		{
			$email = base64_decode($email);
			$password = isset( $_POST['password'] ) ? $_POST['password'] : null;

			if ( $email && Helper::email_validate( $email ) ) {
				$sql = $this->database()->prepare( "SELECT id FROM {$this->table_name} WHERE email = ?" );
				$sql->execute( array( $email ) );
				$user = $sql->fetch( PDO::FETCH_OBJ );

				if ( $user ) {
					$sql = $this->database()->prepare( "UPDATE {$this->table_name} SET password = ?, status = 1 WHERE id = ?" );
					$result = $sql->execute( array( Helper::md5( $password ), $user->id ) );
					
					return $result;
				}
			}

			return false;
		}

		private function login_session( $user )
		{
			if ( $user ) {
				$this->session( 'userid', base64_encode( $user->id ) );
				$this->session( 'username', base64_encode( $this->post_name ? $this->post_name : $user->name ) );
				$this->session( 'useremail', base64_encode( $user->email ) );

				return true;
			}
			else
				return false;
		}

		public function kill_cookie_security()
		{
			setcookie( 'SECURITY', null, ( time()-( 3600*24 ) ), '/', $_SERVER['SERVER_NAME'] );
			setcookie( 'SECURITY_TIME', null, ( time()-( 3600*24 ) ), '/', $_SERVER['SERVER_NAME'] );
		}

		public function favorites()
		{
			$genders = $this->UserGender->ids_by_user( $this->userid() );
			
			return (object)array( 'genders' => $genders );
		}

		public function get_admins()
		{
			$permission = 1;
			$sql = $this->database()->prepare( "SELECT u.name, u.email FROM admin_user au JOIN {$this->table_name} u ON u.id = au.user_id WHERE au.admin_permission_id = ?" );
			$sql->execute( array( $permission ) );

			return $sql->fetchAll( PDO::FETCH_OBJ );
		}
	}
?>