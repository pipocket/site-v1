<?php
    class RateModel extends AppModel
	{
		protected $table_name = "rate";
		protected $table_key = "id";

		public function number_of_votes( $movie_id )
		{
			$sql = $this->database()->prepare( "SELECT COUNT(*) as n FROM {$this->table_name} WHERE movie_id = ?" );
			$sql->execute(array( $movie_id) );
			$total = $sql->fetch( PDO::FETCH_OBJ )->n;

			return $total;
		}

		public function comments_by( $table, $related, $id, $comment_short = false )
		{
			$sql = $this->database()->prepare( "SELECT * FROM {$this->table_name} WHERE {$table}_id = ?" );
			$sql->execute( array($id) );
			$rates = $sql->fetchAll( PDO::FETCH_OBJ );
			$comments = $this->Comment->by_rate( $rates, $related, $comment_short );

			return $comments;
		}

		public function add( $user_id = null, $movie_id = null, $value = null, $comment = null )
		{
			$total_votes = 0;
			$user_id = $user_id?: base64_decode( $this->session( 'userid' ) );
			$value = $value ?: ( int )( ( float )$_POST['score'] * 2 );
			$foursquare_checkin_id = isset( $_POST['foursquare_checkin_id'] ) ? $_POST['foursquare_checkin_id'] : null;
			$foursquare_checkin_place = isset( $_POST['foursquare_checkin_place'] ) ? $_POST['foursquare_checkin_place'] : null;
			$ip = $_SERVER['REMOTE_ADDR'];

			if ( !$movie_id )
				$movie_id = isset( $_POST['movie_id'] ) ? base64_decode( $_POST['movie_id'] ) : null;

			if ( !$movie_id )
				return false;

			try {
				$database = $this->database();

				$sql = $this->database()->prepare( "SELECT COUNT(*) as n FROM {$this->table_name} WHERE movie_id = ? AND user_id = ?" );
				$sql->execute( array( $movie_id, $user_id ) );

				if ( $sql->fetch( PDO::FETCH_OBJ )->n > 0 )
					return 'voted';

				$sql = $database->prepare( "INSERT INTO {$this->table_name} (value, user_id, movie_id, foursquare_checkin_id, foursquare_checkin_place, ip) VALUES (?,?,?,?,?,?)" );
				$sql->execute( array( $value, $user_id, $movie_id, $foursquare_checkin_id, $foursquare_checkin_place, $ip ) );
				$rate_id = $database->lastInsertId();

				$sql = $this->database()->prepare( "SELECT SUM(`value`) AS total, COUNT(*) AS votes FROM rate WHERE movie_id = ?" );
				$sql->execute( array( $movie_id ) );
				$data = $sql->fetch( PDO::FETCH_OBJ );

				// Comment
				$this->Comment->add( $rate_id, $value, $movie_id, $foursquare_checkin_place, $comment );

				// Viewed movie
				$this->UserMovie->add( $movie_id );

				// Remove next movie, if exists
				$this->UserNextMovie->remove( $movie_id );

				return $this->Movie->update_rate( $movie_id, floor( ( $data->total/$data->votes ) ) );
			}
			catch ( Exception $e ) {
				return false;
			}
		}

		public function voted( $movie_id, $user_id )
		{
			if ( $user_id )
				$user_id = base64_decode( $user_id );
			else
				$user_id = base64_decode( $this->session( 'userid' ) );

			$sql = $this->database()->prepare( "SELECT value FROM {$this->table_name} WHERE user_id = ? AND movie_id = ?" );
			$sql->execute( array( $user_id, $movie_id ) );
			$result = $sql->fetch( PDO::FETCH_OBJ );

			return $result ? $result->value : false;
		}

		public function checkin_list()
		{
			$user_id = base64_decode( $this->session( 'userid' ) );
			$list = array();

			$sql = $this->database()->prepare( "SELECT foursquare_checkin_id as checkin_id FROM {$this->table_name} WHERE user_id = ? AND foursquare_checkin_id is not null" );
			$sql->execute( array( $user_id ) );
			$checkins = $sql->fetchAll( PDO::FETCH_OBJ );

			foreach ($checkins as $checkin) {
				$list[$checkin->checkin_id] = true;
			}

			return $list;
		}

		public function minimum_common()
		{
			$movies = $this->Movie->ranking_movie();
			$ids = null;
			$where = null;
			$sum = 0;

			foreach ( $movies as $movie )
				$ids .= " movie_id = {$movie->id} OR ";

			if ( $ids )
				$where = 'WHERE ' . substr( $ids, 0, -3 );

			$sql = $this->database()->prepare( "SELECT COUNT(*) as votes FROM {$this->table_name} {$where}" );
			$sql->execute();

			// N of votes
			$votes = (int)$sql->fetch( PDO::FETCH_OBJ )->votes;

			// N of movies
			$movies = count( $movies );

			// Average
			$average = ( $votes/$movies );

			// Reduce a percentage of average
			$definition = ceil( $average - $this->percent( $votes, $average ) );

			return $definition;
		}

		private function percent( $number_of_votes, $average )
		{
			switch ( $number_of_votes ) {
				case $number_of_votes <= 200:
					// 50%
					return ( $average * 0.5 );
					break;

				case $number_of_votes <= 1000:
					// 25%
					return ( $average * 0.25 );
					break;

				case $number_of_votes <= 10000:
					// 10%
					return ( $average * 0.1 );
					break;

				case $number_of_votes > 10000:
					// 5%
					return ( $average * 0.05 );
					break;
			}
		}

		public function update_all()
		{
			$database = $this->database();
			$movies_votes = array();

			$sql = $database->prepare( "SELECT id, rate FROM movie WHERE rate > 0" );
			$sql->execute();
			$movies = $sql->fetchAll( PDO::FETCH_OBJ );

			foreach ( $movies as $movie ) {
				$sql = $database->prepare( "SELECT SUM(`value`) AS total, COUNT(*) AS votes FROM rate WHERE movie_id = ?" );
				$sql->execute( array( $movie->id ) );
				$data = $sql->fetch( PDO::FETCH_OBJ );

				$this->Movie->update_rate( $movie->id, floor( ( $data->total/$data->votes ) ) );
				// $movies_votes[$movie->id] = floor( ( $data->total/$data->votes ) ) . ' <-> ' . $movie->rate;
			}

			echo "O rate de todos os filmes foram atualizados.";
		}
	}
?>