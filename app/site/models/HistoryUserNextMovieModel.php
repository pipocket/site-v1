<?php
	class HistoryUserNextMovieModel extends AppModel
	{
		protected $table_name = "history_user_next_movie";
		protected $table_key = "id";

		public function add( $movie_id )
		{
			try {
				$sql = $this->database()->prepare( "INSERT INTO {$this->table_name} (user_id, movie_id) VALUES (?,?)" );
				return $sql->execute( array( $this->userid(), $movie_id ) );
			}
			catch( exception $e ) {
				return false;
			}
		}
	}
?>