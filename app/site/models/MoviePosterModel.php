<?php
    class MoviePosterModel extends AppModel
	{
		protected $table_name = "movie_poster";
		protected $table_key = "";
		
		public function by_movie( $movie_id )
		{
			$sql = $this->database()->prepare( "SELECT * FROM {$this->table_name} WHERE movie_id = ?" );
			$sql->execute( array( $movie_id ) );

			return $sql->fetchAll( PDO::FETCH_OBJ );
		}
	}
?>