<?php
    class UserPreferenceModel extends AppModel
	{
		protected $table_name = "user_preference";
		protected $table_key = "user_id";
		
		public function add( $user_id, $twitter_post = false, $facebook = false )
		{
			$twitter_post = $twitter_post ? 0 : 0;
			$facebook = $facebook ? 1 : 0;

			$sql = $this->database()->prepare( "INSERT INTO {$this->table_name} (user_id, twitter_post, facebook_post, facebook_default) values (?,?,?,?)" );
			return $sql->execute( array( $user_id, $twitter_post, $facebook, $facebook ) );
		}

		public function by_user()
		{
			$id = base64_decode( $this->session( 'userid' ) );
			$preferences = $this->getByUser_id($id);

			return $preferences['data'];
		}

		public function update()
		{
			if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
				$id = base64_decode( $this->session( 'userid' ) );
				$facebook_post = isset( $_POST['facebook_post'] ) ? 1 : 0;
				$facebook_link = isset( $_POST['facebook_link'] ) ? 1 : 0;
				$facebook_default = isset( $_POST['facebook_default'] ) ? 1 : 0;
				$twitter_post = isset( $_POST['twitter_post'] ) ? 1 : 0;
				$twitter_link = isset( $_POST['twitter_link'] ) ? 1 : 0;
				$twitter_default = isset( $_POST['twitter_default'] ) ? 1 : 0;
				$foursquare_last_checkins = isset( $_POST['foursquare_last_checkins'] ) ? 1 : 0;

				if ( isset( $_POST['facebook_update'] ) AND $_POST['facebook_update'] )
					$this->User->update_facebook_id();

				if ( $twitter_post === 0 )
					$this->session( 'access_token', NULL );

				$sql = $this->database()->prepare( "UPDATE {$this->table_name} SET facebook_post = ?, facebook_link = ?, twitter_post = ?, twitter_link = ?, foursquare_last_checkins = ?, facebook_default = ?, twitter_default = ? WHERE user_id = ?" );
				$result = $sql->execute( array( $facebook_post, $facebook_link, $twitter_post, $twitter_link, $foursquare_last_checkins, $facebook_default, $twitter_default, $id ) );

				return $result;
			}
		}
	}
?>