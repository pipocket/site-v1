<?php
    class CommentModel extends AppModel
	{
		protected $table_name = "comment";
		protected $table_key = "";

		public function by_rate( $rates, $related, $comment_short )
		{
			$rates_id = null;
			$comments = null;

			foreach ( $rates as $rate ) {
				$rates_id .= "rate_id = {$rate->id} OR ";
			}

			if ( $rates_id ) {
				$rates_id = substr($rates_id, 0, -4);

				$sql = $this->database()->prepare( "SELECT *, rate.user_id, rate.movie_id, rate.value as rate FROM {$this->table_name} JOIN rate ON rate.id = rate_id WHERE $rates_id ORDER BY {$this->table_name}.`date` DESC" );
				$sql->execute();
				$comments = $sql->fetchAll( PDO::FETCH_OBJ );

				foreach ( $comments as $comment ) {
					if ( $related == 'user' )
						$comment->user_data = $this->User->data( $comment->user_id, 'name' );
					elseif ( $related == 'movie' ) {
						$comment->movie_data = $this->Movie->data( $comment->movie_id, 'name, `key`' );
						$comment->movie_data->fullname = $comment->movie_data->name;
						$label = "Sobre {$comment->movie_data->name}";

						if ( strlen( $label )  > 38 ) {
							$label = mb_substr( $label, 0, 37, 'utf-8' );
							$comment->movie_data->name = end( explode( 'Sobre ', $label ) ) . '...';
						}
					}

					$comment->datebr = Helper::date_format('d/m/Y \à\s H:i', $comment->date);

					if ( $comment_short )
						$this->slice_text( $comment );
				}
			}

			return $comments;
		}

		public function add( $rate_id, $rate, $movie_id, $foursquare_checkin_place, $comment)
		{
			$movie = $this->Movie->getById( $movie_id );
			$user_id  = base64_decode( $this->session( 'userid' ) );
			$facebook = isset($_POST['facebook']) ? 1 : 0;
			$twitter = isset($_POST['twitter']) ? 1 : 0;

			if ( $comment )
				$description = trim( $comment );
			else
				$description = isset( $_POST['comment'] ) ? trim( $_POST['comment'] ) : null;

			// Post
			if ( $description && 
				 substr( $description, strlen($description)-1, 1 ) !== '.' && 
				 substr( $description, strlen($description)-1, 1 ) !== '!' && 
				 substr( $description, strlen($description)-1, 1 ) !== '?'
				) {
				$description .= '.';
			}

			$message = $description ? $description . ' ' : null;
			$message .= "Nota {$rate}.";

			if ( $facebook === 1 )
				$this->facebook_feed( $message, $movie['data'], $foursquare_checkin_place );
			if ( $twitter === 1 )
				$this->twitter_post( "O filme '{$movie['data']->name}' merece nota $rate! via @PipocketOficial http://pipocket.com/filme/{$movie['data']->key}" );

			if ( $description )
				$result = $this->set( array( 'data' => array( 'rate_id' => $rate_id, 'description' => $description, 'facebook' => $facebook, 'twitter' => $twitter ) ) );
			else
				$result = true;

			return $result;
		}

		public function last()
		{
			$distincts = array();

			$sql = $this->database()->prepare( 
				"SELECT 
					u.id as user_id, u.name as user, c.description, r.value as rate, m.key as movie_key, m.name as movie
				FROM 
					{$this->table_name} c 
				JOIN 
					rate r ON r.id = c.rate_id 
				JOIN 
					user u ON u.id = r.user_id 
				JOIN 
					movie m ON m.id = r.movie_id 
				WHERE 
					{$this->Movie->rules( 'in-theaters' )}
				ORDER BY 
					c.`date` desc 
				LIMIT 
					15" );

			$sql->execute();
			$comments = $sql->fetchAll( PDO::FETCH_OBJ );

			foreach ( $comments as $key => $comment ) {
				if ( !array_key_exists( $comment->user_id, $distincts ) AND count( $distincts ) < 3 ) {
					$this->slice_text( $comment );
					$comment->title = $comment->movie;
					$label = "{$comment->user} sobre {$comment->movie}";

					if ( strlen( $label )  > 38 ) {
						$label = mb_substr( $label, 0, 37, 'utf-8' );
						$comment->movie = end( explode( ' sobre ', $label ) ) . '...';
					}

					$distincts[$comment->user_id] = $comment;
				}
			}
			
			return $distincts;
		}

		public function all( $length )
		{
			$distincts = array();

			$sql = $this->database()->prepare( 
				"SELECT 
					u.id as user_id, u.name as user, c.description, r.value as rate, m.key as movie_key, m.name as movie
				FROM 
					{$this->table_name} c 
				JOIN 
					rate r ON r.id = c.rate_id 
				JOIN 
					user u ON u.id = r.user_id 
				JOIN 
					movie m ON m.id = r.movie_id 
				WHERE 
					{$this->Movie->rules( 'in-theaters' )}
				ORDER BY 
					c.`date` desc 
				LIMIT 
					$length" );

			$sql->execute();
			$comments = $sql->fetchAll( PDO::FETCH_OBJ );
			
			return $comments;
		}
	}
?>