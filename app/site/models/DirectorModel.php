<?php
    class DirectorModel extends AppModel
	{
		protected $table_name = "director";
		protected $table_key = "id";
		
		public function by_name( $directors )
		{
			$directors = explode( ',', $directors );
			$where = null;

			foreach ( $directors as $name )
				$where .= "name = '$name' OR ";
			
			$where = substr( $where, 0, -3 );

			$sql = $this->database()->prepare( "SELECT id FROM {$this->table_name} WHERE $where" );
			$sql->execute();

			return $sql->fetchAll( PDO::FETCH_OBJ );
		}
	}
?>