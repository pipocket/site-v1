<?php
    class ActorModel extends AppModel
	{
		protected $table_name = "actor";
		protected $table_key = "id";
		
		public function by_name( $actors )
		{
			$actors = explode( ',', $actors );
			$where = null;

			foreach ( $actors as $name )
				$where .= "name = '$name' OR ";
			
			$where = substr( $where, 0, -3 );

			$sql = $this->database()->prepare( "SELECT id FROM {$this->table_name} WHERE $where" );
			$sql->execute();

			return $sql->fetchAll( PDO::FETCH_OBJ );
		}
	}
?>