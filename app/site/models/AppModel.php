<?php
    class AppModel extends aura_model
	{
		public function related( $field, $id, $field_related = 'name', $object = false )
		{
			$items = null;
			
			$sql = $this->database()->prepare( "SELECT r.*, tr.$field_related FROM {$this->table_name} r JOIN {$this->table_related} tr ON tr.id = r.{$this->table_related}_id WHERE $field = ?" );
			$sql->execute( array( $id ) );
			$result = $sql->fetchAll( PDO::FETCH_OBJ );

			foreach( $result as $item ) {
				$items .= $item->$field_related . ', ';
			}

			if ( !$object )
				return substr( $items, 0, -2 );
			else
				return $result;
		}

		public function related_wlink( $field, $id, $field_related, $optional_where = null )
		{
			$optional_where = $optional_where ? " AND $optional_where" : $optional_where;
			
			$sql = $this->database()->prepare( "SELECT r.*, tr.{$field_related[0]}, tr.{$field_related[1]} FROM {$this->table_name} r JOIN {$this->table_related} tr ON tr.id = r.{$this->table_related}_id WHERE $field = ? $optional_where" );
			$sql->execute( array( $id ) );
			
			return $sql->fetchAll( PDO::FETCH_OBJ );
		}

		public function slice_text( $comment )
		{
			if ( strlen( $comment->description ) > 60 ) {
				$part1 = null;
				$part2 = null;

				if( substr( $comment->description, 60, 1 ) != ' ' ) {
					$part1 = substr( $comment->description, 0, 60 );
					$part2 = explode( ' ', substr( $comment->description, 60, strlen( $comment->description ) ) );

					$part1 .= $part2[0];
					unset($part2[0]);
					$part2 = join( ' ', $part2 );
				}
				else {
					$part1 = substr( $comment->description, 0, 60 );
					$part2 = substr( $comment->description, 60, strlen( $comment->description ) );
				}

				$comment->description = $part1;

				if ( $part2 )
					$comment->description .= "<a href='#' rel='more'> mais</a><span class='hide'> 	" . $part2;
				
				$comment->description .= "</span>";
			}
		}

		private function facebook()
		{
			$facebook_oauth = array( 'appId'  => FACEBOOK_APP_ID, 'secret' => FACEBOOK_SECRET_ID );
			$facebook = $this->vendors( 'facebook', $facebook_oauth, 'array', true );

			return $facebook;
		}

		private function request_curl( $url, $post = false )
		{
			$ch = curl_init();
			curl_setopt( $ch, CURLOPT_URL, $url );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );

			if ( $post )
				curl_setopt( $ch, CURLOPT_POST, 1 );
			
			$data = curl_exec( $ch );
			curl_close( $ch );

			return $data;
		}

		public function facebook_data()
		{
			$facebook = $this->facebook();

			if ( $facebook->getUser() != 0 ) {
				$data = $this->request_curl( "https://graph.facebook.com/{$facebook->getUser()}?access_token={$facebook->getAccessToken()}" );
				$data = json_decode( $data, true );

				if ( !isset( $data['error'] ) ) {
					$data['birthday'] = isset($data['birthday']) ? $data['birthday'] : null;

					if ( $data['birthday'] ) {
						$data['birthday'] = explode( '/', $data['birthday'] );
						$data['birthday'] = $data['birthday'][2] . '-' . $data['birthday'][0] . '-' . $data['birthday'][1];
					}
				}
				else 
					Helper::log( 'facebook', $data['error']['message'] );

				return $data;
			}
			else
				return false;

		}

		public function facebook_check_permissions()
		{
			$facebook = $this->facebook();
			$this->User->update_facebook_id();
			
			$data = $this->request_curl( "https://graph.facebook.com/{$facebook->getUser()}/permissions?access_token={$facebook->getAccessToken()}" );
			
			$permissions = json_decode( $data, true );
			return isset($permissions['data'][0]['publish_stream']);
		}

		public function facebook_feed( $message, $movie, $foursquare_checkin_place )
		{
			$facebook = $this->facebook();
			$user = $this->User->data( false, 'facebook' );
			$caption = 'pipocket.com';

			if( $facebook->getUser() != 0 AND ( $facebook->getUser() != $user->facebook ) ) {
				$facebook->destroySession();
			}

			try {
				$facebook->api( "/{$user->facebook}/feed/", 'POST', array( 'message' => $message, 'link' => "http://pipocket.com/filme/{$movie->key}", 'name' => $movie->name, 'caption' => $caption, 'picture' => "http://media-files.{$caption}/movies/{$movie->id}_90x90.jpg" ) );
			}
			catch( exception $e ) {
				Helper::log( 'facebook', $e );
			}
		}

		public function twitter_post( $message )
		{
			$user_data = $this->User->data( null, 'twitter_oauth_token, twitter_oauth_token_secret' );

			if ( $user_data ) {
				$params = array( TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, $user_data->twitter_oauth_token, $user_data->twitter_oauth_token_secret );
				$connection = $this->vendors( 'TwitterOAuth', $params );

				$connection->post( 'statuses/update', array( 'status' => $message ) );

				return true;
			}
		}

		public function userid()
		{
			return base64_decode( $this->session( 'userid' ) );
		}

		protected function security()
		{
			if (!$this->session( 'userid' ) OR !$this->session( 'username' ))
				return false;
			else
				return true;
		}
	}
?>