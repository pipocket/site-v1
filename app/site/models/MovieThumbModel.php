<?php
    class MovieThumbModel extends AppModel
	{
		protected $table_name = "movie_thumb";
		protected $table_key = "";
		
		public function by_movie( $movie_id, $field )
		{
			$sql = $this->database()->prepare( "SELECT $field FROM {$this->table_name} WHERE movie_id = ?" );
			$sql->execute( array( $movie_id ) );
			$data = $sql->fetch( PDO::FETCH_OBJ );

			return $data ? $data->$field : null;
		}
	}
?>