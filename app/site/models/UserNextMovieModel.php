<?php
    class UserNextMovieModel extends AppModel
	{
		protected $table_name = "user_next_movie";
		protected $table_key = "";
		protected $table_related  = "movie";
		
		public function by_user( $user_id )
		{
			return $this->related_wlink( 'user_id', $user_id, array('name', 'key') );
		}

		public function by_movie( $movie_id )
		{
			$user_id = $this->userid() ? $this->userid() : 0;
			$next = $this->related_wlink( 'movie_id', $movie_id, array('name', 'key'), "r.user_id = $user_id" );

			return count( $next ) == 0 ? false : true;
		}

		public function add( $movie_id )
		{
			try {
				$sql = $this->database()->prepare( "INSERT INTO {$this->table_name} (user_id, movie_id) VALUES (?,?)" );
				$status = $sql->execute( array( $this->userid(), $movie_id ) );

				if ( $status )
					$this->HistoryUserNextMovie->add( $movie_id );

				return $status;
			}
			catch( exception $e ) {
				return false;
			}
		}

		public function remove( $movie_id )
		{
			try {
				$sql = $this->database()->prepare( "DELETE FROM {$this->table_name} WHERE user_id = ? AND movie_id = ?" );
				return $sql->execute( array( $this->userid(), $movie_id ) );
			}
			catch( exception $e ) {
				return false;
			}
		}
	}
?>