<?php
    class GenderModel extends AppModel
	{
		protected $table_name = "gender";
		protected $table_key = "id";
		
		public function all()
		{
			$genders = $this->getAll();

			return $genders['data'];
		}

		public function by_description( $genders )
		{
			$genders = explode( ',', $genders );
			$where = null;

			foreach ( $genders as $description )
				$where .= "description = '$description' OR ";
			
			$where = substr( $where, 0, -3 );

			$sql = $this->database()->prepare( "SELECT id FROM {$this->table_name} WHERE $where" );
			$sql->execute();

			return $sql->fetchAll( PDO::FETCH_OBJ );
		}
	}
?>