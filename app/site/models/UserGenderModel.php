<?php
    class UserGenderModel extends AppModel
	{
		protected $table_name = "user_gender";
		protected $table_key = "";
		protected $table_related = "gender";
		
		public function by_user( $user_id )
		{
			return $this->related( 'user_id', $user_id, 'description', true );
		}

		public function ids_by_user()
		{
			$sql = $this->database()->prepare( "SELECT gender_id as id FROM {$this->table_name} WHERE user_id = ?" );
			$sql->execute( array( $this->userid() ) );

			return $sql->fetchAll( PDO::FETCH_OBJ );
		}

		public function add( $gender_id )
		{
			try {
				$sql = $this->database()->prepare( "INSERT INTO {$this->table_name} (user_id, gender_id) VALUES (?,?)" );
				return $sql->execute( array( $this->userid(), $gender_id ) );
			}
			catch( exception $e ) {
				return false;
			}
		}

		public function remove( $gender_id )
		{
			try {
				$sql = $this->database()->prepare( "DELETE FROM {$this->table_name} WHERE user_id = ? AND gender_id = ?" );
				return $sql->execute( array( $this->userid(), $gender_id ) );
			}
			catch( exception $e ) {
				return false;
			}
		}
	}
?>