<?php
    class MovieGenderModel extends AppModel
	{
		protected $table_name = "movie_gender";
		protected $table_key = "";
		protected $table_related = "gender";
		
		public function by_movie( $movie_id )
		{
			$genders = $this->related_wlink( 'movie_id', $movie_id, array( 'id', 'description' ) );
			$user_genders = $this->UserGender->ids_by_user( $this->userid() );

			foreach ( $user_genders as $gender_user ) {
				foreach ( $genders as $gender ) {
					if ( $gender_user->id == $gender->gender_id )
						$gender->favorite = true;
				}
			}
			
			return $genders;
		}
	}
?>