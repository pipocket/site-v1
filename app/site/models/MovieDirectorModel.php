<?php
    class MovieDirectorModel extends AppModel
	{
		protected $table_name = "movie_director";
		protected $table_key = "";
		protected $table_related = "director";

		public function by_movie( $movie_id )
		{
			$directors = $this->related_wlink( 'movie_id', $movie_id, array( 'id', 'name' ) );
			$user_directors = $this->UserDirector->ids_by_user();

			foreach ( $user_directors as $director_user ) {
				foreach ( $directors as $director ) {
					if ( $director_user->id == $director->director_id )
						$director->favorite = true;
				}
			}

			return $directors;
		}
	}
?>