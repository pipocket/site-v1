<?php
    class MovieActorModel extends AppModel
	{
		protected $table_name = "movie_actor";
		protected $table_key = "";
		protected $table_related = "actor";

		public function by_movie( $movie_id )
		{
			$actors = $this->related_wlink( 'movie_id', $movie_id, array('id', 'name') );
			$user_actors = $this->UserActor->ids_by_user();

			foreach ( $user_actors as $actor_user ) {
				foreach ( $actors as $actor ) {
					if ( $actor_user->id == $actor->actor_id )
						$actor->favorite = true;
				}
			}

			return $actors;
		}
	}
?>