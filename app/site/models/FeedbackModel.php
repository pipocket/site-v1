<?php
    class FeedbackModel extends AppModel
	{
		protected $table_name = "feedback";
		protected $table_key = "id";
		
		public function save()
		{
			$comment = isset( $_POST['comment'] ) ? $_POST['comment'] : false;

			if ( !$comment )
				return false;

			$data = array( 'data' => array( 'user_id' => $this->userid(), 'comment' => $comment ) );
			return $this->set( $data );
		}
	}
?>