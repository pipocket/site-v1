<?php
    class ProfileController extends AppController
	{
		public function index( $id = null )
		{
			$user = $this->User->one( $id );

			if ( !$user && !$id )
				$this->redirect("/{$this->locale_data['PATH_JOIN']}");
			elseif ( !$user && $id ) {
				$this->redirect( '/' );
			}

			$this->set('user', $user);
			$this->show('index');
		}

		public function informations()
		{
			$twitter_success = false;
			$twitter_error = false;
			$foursquare_success = false;
			$foursquare_error = false;
			$error = false;
			$success = false;
			$success_preferences = false;
			$twitter_denied = false;

			$this->protect_page();
			$informations = $this->UserPreference->by_user();

			if ( $this->message( 'basic_error' ) ) {
				$error = $this->message( 'basic_error' );
				$this->message( 'basic_error', 'null', time() );
			}
			
			if ( $this->message( 'basic_success' ) ) {
				$success = $this->message( 'basic_success' );
				$this->message( 'basic_success', 'null', time() );
			}
			
			if ( $this->message( 'preferences_success' ) ) {
				$success_preferences = $this->message( 'preferences_success' );
				$this->message( 'preferences_success', 'null', time() );
			}
			
			if ( $error ) {
				$this->set( 'error_password', $this->message( 'register_validate_password' ) );
				$this->set( 'error_maxlength_password', $this->message( 'register_validate_maxlength_password' ) );
				$this->set( 'error_email_in_use', $this->message( 'register_validate_email_in_use' ) );
			}

			if ( $this->message( 'twitter_success' ) ) {
				$twitter_success = $this->message( 'twitter_success' );
				$this->message( 'twitter_success', 'null', time() );
			}

			if ( $this->message( 'twitter_error' ) ) {
				$twitter_error = $this->message( 'twitter_error' );
				$this->message( 'twitter_error', 'null', time() );
			}

			if ( $this->message( 'twitter_denied' ) ) {
				$twitter_denied = $this->message( 'twitter_denied' );
				$this->message( 'twitter_denied', 'null', time() );
			}

			if ( $this->message( 'foursquare_success' ) ) {
				$foursquare_success = $this->message( 'foursquare_success' );
				$this->message( 'foursquare_success', 'null', time() );
			}

			if ( $this->message( 'foursquare_error' ) ) {
				$twitter_error = $this->message( 'foursquare_error' );
				$this->message( 'foursquare_error', 'null', time() );
			}

			$this->set( 'success', $success );
			$this->set( 'success_preferences', $success_preferences );
			$this->set( 'twitter_success', $twitter_success );
			$this->set( 'twitter_error', $twitter_error );
			$this->set( 'twitter_denied', $twitter_denied );
			$this->set( 'foursquare_success', $foursquare_success );
			$this->set( 'foursquare_error', $foursquare_error );
			$this->set( 'error', $error );
			$this->set('informations', $informations);
			$this->show('informations');
		}

		public function basic()
		{
			$result = $this->User->update();

			if ( $result ) {
				$this->message( 'basic_error', false );
				$this->message( 'basic_success', true );
			}
			else {
				$this->message( 'basic_error', true );
				$this->message( 'basic_success', false );
			}

			$this->redirect( '/' . $this->locale_data['PATH_PROFILE_INFO'] );
		}

		public function preferences()
		{
			$result = $this->UserPreference->update();
			$this->ajax($result);
		}

		public function twitter_authorize()
		{
			$access_token = $this->session( 'access_token' );

			if ( !$this->session( 'access_token' ) OR empty( $access_token ) OR isset( $access_token[' '] ) ) {
			    	$this->User->twitter_connect( 'verify/' );
			    	exit;
			}
			
			$result = $this->User->update_twitter_id();
			
			if ( $result )
				$this->message( 'twitter_success', true );
			else {
				$this->message( 'twitter_error', true );
				$this->User->twitter_clear();
			}

			$this->redirect( '/' . $this->locale_data['PATH_PROFILE_INFO'] );
		}

		public function twitter_denied()
		{
			$this->message( 'twitter_error', true );
			$this->message( 'twitter_denied', true );

			$this->User->twitter_clear();
			$this->redirect( '/' . $this->locale_data['PATH_PROFILE_INFO'] );
		}

		public function twitter_callback( $verify, $url_redirect = null )
		{
			if ( $verify == 'callback' ) {
				$url_redirect = $url_redirect ? '/profile/twitter_authorize' : null;
				$this->User->twitter_callback( $url_redirect );
				exit;
			}
		}

		public function foursquare_authorize()
		{
			$request = explode( '?code=', $_SERVER['REQUEST_URI'] );
			$code = count( $request ) > 1 ? end( $request ) : null;
			
			if ( !$code ) {
				$result = $this->User->foursquare( $code );
				exit;
			}

			$content = $this->User->foursquare( $code );
			$result = $this->User->update_foursquare_token( $content );
			
			if ( $result )
				$this->message( 'foursquare_success', true );
			else {
				$this->message( 'foursquare_error', true );
			}

			$this->redirect( '/' . $this->locale_data['PATH_PROFILE_INFO'] );
		}

		public function last_checkins()
		{
			$checkins = $this->User->last_checkins();
			$checkins = json_decode($checkins);
			
			$checkins = isset( $checkins->response->checkins->items ) ? $checkins->response->checkins->items : null;

			if ( $checkins ) {
				$checkin_list = $this->Rate->checkin_list();
				
				foreach ( $checkins as $key => $checkin ) {
					unset( $checkin->venue->categories );
					unset( $checkin->venue->comments );
					unset( $checkin->venue->verified );
					unset( $checkin->venue->stats );
					unset( $checkin->venue->contact );
					unset( $checkin->photos );
					unset( $checkin->comments );
					unset( $checkin->source );

					if ( array_key_exists($checkin->id, $checkin_list) )
						unset( $checkins[$key] );
				}
			}

			$this->ajax( $checkins );
		}

		public function favorite()
		{
			$type = isset( $_POST['type'] ) ? $_POST['type'] : null;
			$action = isset( $_POST['action'] ) ? $_POST['action'] : null;
			$id = isset( $_POST['id'] ) ? $_POST['id'] : null;
			$result = false;

			switch ( $type ) {
				case 'gender':
					if ( $action == 'favorite' )
						$result = $this->UserGender->add( $id );
					elseif ($action == 'unfavorite')
						$result = $this->UserGender->remove( $id );
					break;

				case 'director':
					if ( $action == 'favorite' )
						$result = $this->UserDirector->add( $id );
					elseif ($action == 'unfavorite')
						$result = $this->UserDirector->remove( $id );
					break;

				case 'actor':
					if ( $action == 'favorite' )
						$result = $this->UserActor->add( $id );
					elseif ($action == 'unfavorite')
						$result = $this->UserActor->remove( $id );
					break;

				case 'movie':
					if ( $action == 'favorite' )
						$result = $this->UserMovie->favorite( $id );
					elseif ($action == 'unfavorite')
						$result = $this->UserMovie->unfavorite( $id );
					break;
			}

			$this->ajax( $result );
		}

		public function next_movie()
		{
			$action = isset( $_POST['action'] ) ? $_POST['action'] : null;
			$id = isset( $_POST['id'] ) ? $_POST['id'] : null;
			$result = false;

			if ( $action == 'check' )
				$result = $this->UserNextMovie->add( $id );
			elseif ($action == 'uncheck')
				$result = $this->UserNextMovie->remove( $id );

			$this->ajax( $result );
		}
	}
?>