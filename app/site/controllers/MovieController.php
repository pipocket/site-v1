<?php
    class MovieController extends AppController
	{
		public function index( $key = null )
		{
			if ( !$key )
				$this->redirect( '/' . $this->locale_data['PATH_SEARCH'] );

			$movie = $this->Movie->one( $key );

			if ( !$movie )
				$this->redirect( '/' . $this->locale_data['PATH_SEARCH'] );

			$this->set( 'movie', $movie );
			$this->show( 'index' );
		}
	}
?>