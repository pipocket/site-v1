<?php
    class ServicesController extends AppController
	{
		public function movie( $key )
		{
			$movie = $this->Movie->one( $key, false, $this->get_request('user_id') );

			unset( $movie->id );
			unset( $movie->date );
			unset( $movie->next );
			unset( $movie->poster );
			unset( $movie->preview );
			unset( $movie->revision );
			unset( $movie->thumb );
			unset( $movie->status );
			unset( $movie->storyline_check );
			unset( $movie->thumb_facebook );
			unset( $movie->user_preferences );

			if ( $movie->age == 1 )
				$movie->age = '';
			else if ( $movie->age == 'L' )
				$movie->age = '- Livre';
			else
				$movie->age = '- '. $movie->age .' Anos';

			$this->json( $movie );
		}

		public function movies()
		{
			$data = array();

			switch ( $this->get_request('type') ) {
				case 'release':
					$text_search = $this->get_request('text_search');
					$movies = $this->Movie->release( 10, true, true, $text_search );

					foreach ( $movies as $key => $movie ) {
						$data[$key]->id = $this->base64( $movie->id );
						$data[$key]->name = $movie->name;
						$data[$key]->time = $movie->time .' Minutos';
						$data[$key]->key = $movie->key;
						$data[$key]->directors = $movie->directors;
						$data[$key]->thumb2 = $movie->thumb;
						$data[$key]->genders = $movie->gender;

						if ( $movie->age == 1 )
							$data[$key]->age = '';
						else if ( $movie->age == 'L' )
							$data[$key]->age = '/ Livre';
						else
							$data[$key]->age = '/ '. $movie->age .' Anos';
					}

					break;

				case 'in_theaters':
					$text_search = $this->get_request('text_search');
					$movies = $this->Movie->search( $text_search, null, null, null, 'em-cartaz', 10 );

					foreach ( $movies as $key => $movie ) {
						$data[$key]->id = $this->base64( $movie->id );
						$data[$key]->name = $movie->name;
						$data[$key]->key = $movie->key;
						$data[$key]->thumb2 = $movie->thumb2;
						$data[$key]->rate = $movie->rate;
						$data[$key]->time = $movie->time .' Minutos';
					}

					break;
			}

			$this->json( $data );
		}

		public function movie_ranking()
		{
			$movies = $this->Movie->ranking();

			foreach ( $movies as $key => $movie ) {
				$data[$key]->id = $this->base64( $movie->id );
				$data[$key]->name = $movie->name;
				$data[$key]->key = $movie->key;
				$data[$key]->banner = $movie->banner;
			}

			$this->json( $data );
		}

		public function comments()
		{
			$comments = $this->Comment->all( 10 );
			$this->json( $comments );
		}

		public function vote()
		{
			$movie_id 	= $this->get_request( 'movie' );
			$user_id 	= $this->get_request( 'user_id' );
			$rate 		= $this->get_request( 'rate' );
			$comment 	= $this->get_request( 'comment' );

			$status = $this->Rate->add( $this->base64( $user_id, 'decode' ), $this->base64( $movie_id, 'decode' ), $rate, urldecode($comment) );

			if ( $status === 'voted' )
				$status = array('status'=>false, 'message'=>'voted');
			else if ( $status === true )
				$status = array('status'=>true, 'message'=>'');
			else
				$status = array('status'=>false, 'message'=>'invalid');

			$this->json( $status );
		}

		private function json( $data )
		{
			header('Cache-Control: no-cache, must-revalidate');
			header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
			header('Content-type: application/json');

			if ( $this->get_request( 'callback' ) )
				echo $this->get_request( 'callback' ) .'('. json_encode( $data ) .')';
			else
				echo json_encode( $data );
		}

		private function base64( $key, $action = 'encode' )
		{
			switch ( $action ) {
				case 'encode':
					$key = base64_encode( $key );
					break;

				case 'decode':
					$key = base64_decode( $key );
					break;
			}

			return $key;
		}
	}
?>