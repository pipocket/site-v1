<?php
    class LoginController extends AppController
	{
		public function __construct()
		{
			parent::__construct( false );
		}
		
		public function index()
		{
			$result = $this->User->login();

			if ( $result )
				echo 'true';
			else
				echo 'false';
		}

		public function out()
		{
			$this->User->kill_cookie_security();
			$this->session( 'destroy' );
			$this->redirect( '/' );
		}
	}
?>