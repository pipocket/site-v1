<?php
    class HomeController extends AppController
	{
		public function index()
		{
			if ( $this->message( 'register_confirmation' ) ) {
				$this->set( 'confirmation', true );
				$this->set( 'confirmation_login_btn', true );

				if ( $this->message( 'register_confirmation' ) == 'already-activated' ) {
					$this->set( 'confirmation_message', '<strong>Ops</strong>, sua conta já esta ativa!' );
					$this->set( 'confirmation_login_btn', false );
				}
				elseif ( $this->message( 'register_confirmation' ) == 'activated' )
					$this->set( 'confirmation_message', '<strong>Parabéns</strong>, sua conta foi ativada com sucesso!' );

				$this->message( 'register_confirmation', false );
			}

			if ( $this->session( 'redefine_error' ) OR $this->session( 'redefine_success' ) ) {
				$this->set( 'redefine_success', $this->session( 'redefine_success' ) );
				$this->set( 'redefine_error', $this->session( 'redefine_error' ) );

				$this->session( 'redefine_success', null );
				$this->session( 'redefine_error', null );
			}

			if ( $this->session( 'redefine_password_mail' ) )
				$this->set( 'redefine_password', true );

			$this->set( 'ranking', $this->Movie->ranking() );
			$this->set( 'releases', $this->Movie->release() );
			$this->set( 'comments', $this->Comment->last() );

			$this->show( 'index' );
		}

		public function new_password()
		{
			$email = $this->session( 'redefine_password_mail' );
			$this->session( 'redefine_password_mail', null );

			$result = $this->User->redefine_password( $email );

			if ( $result ) {
				$this->session( 'redefine_error', false );
				$this->session( 'redefine_success', true );
			}
			else {
				$this->session( 'redefine_error', false );
				$this->session( 'redefine_success', true );
			}

			$this->redirect( '/' );
		}

		public function redefine_password( $email = null )
		{
			if ( !$email )
				$this->redirect( '/' );

			$result = $this->User->check_status_password( $email );

			if ( $result )
				$this->session( 'redefine_password_mail', $email );
			
			$this->redirect( '/' );
		}
	}
?>