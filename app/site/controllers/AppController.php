<?php
    class AppController extends aura_controller
	{
		protected $locale_data;

		public function __construct( $verify = true, $register = false)
		{
			parent::__construct();

			// Set locale language
			$this->locale_data = $this->locale('pt_br', true);

			if ($verify) {
				// Security
				if ( !$this->security() ) {
					$this->session( 'userid', false, true );
					$this->session( 'username', false, true );
					$this->set('logged', false);
				}
				else {
					if ( $register )
						$this->redirect( '/' );

					$this->set( 'logged', true );
					$this->set( 'username', base64_decode( $this->session( 'username' ) ) );
				}
			}

			$this->set( 'current_url', $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );

			$global_config = $this->vendors( 'global_config' );
			$this->set( 'is_production', $global_config->is_production );
		}

		public function protect_page()
		{
			if ( !$this->Security() )
				$this->redirect( '/' );
		}

		protected function security()
		{
			if (!$this->session( 'userid' ) OR !$this->session( 'username' ))
				return false;
			else
				return true;
		}

		public function get_param( $param )
		{
			return isset( $_GET[$param] ) ? $_GET[$param] : null;
		}

		public function mail( $mail_from, $body, $subject )
		{
			$mail = $this->vendors( 'phpmailer' );
			$mail->IsSMTP();

			$mail->Host = AURA_SMTP_HOST;
			$mail->SMTPAuth = AURA_SMTP_AUTH;
			$mail->SMTPSecure = "ssl";
			$mail->SMTPKeepAlive = true;
			$mail->Host = AURA_SMTP_HOST;
			$mail->Port = AURA_SMTP_PORT;
			$mail->Username = AURA_SMTP_USERNAME;
			$mail->Password = AURA_SMTP_PASSWORD;

			$mail->SetFrom( $mail_from, AURA_SMTP_FROMNAME );
			$mail->Subject = $subject;
			$mail->MsgHTML( $body );

			return $mail;
		}

		public function inivar( $var = null )
		{
			if ( $var )
			 echo $this->locale_data[$var];
		}

		public function ajax( $value )
		{
			header('Cache-Control: no-cache, must-revalidate');
			header('Content-type: application/json');

			echo json_encode( $value );
		}

		public function get_request( $name = null )
		{
			$items = array();
			$url = $_SERVER['REQUEST_URI'];
			$url = explode( '?', $url );
			$params = isset( $url[1] ) ? $url[1]: false;

			if ( $params ) {
				$params = explode( '&', $params );

				foreach ( $params as $param ) {
					$param = explode( '=', $param );

					if ( count( $param ) == 2 )
						$items[$param[0]] = $param[1];
				}
			}

			if ( !$name )
				return $items;
			else
				return isset( $items[$name] ) ? $items[$name] : null;
		}
	}
?>