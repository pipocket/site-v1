<?php
    class SignupController extends AppController
	{
		public function __construct()
		{
			parent::__construct( true, true );
		}

		public function index()
		{
			$this->show('index');
		}

		public function facebook()
		{
			$this->User->facebook();
			$this->redirect( '/' );
		}

		public function twitter( $action = null, $url_redirect = false )
		{
			if ( $action == 'callback' ) {
				$url_redirect = $url_redirect ? '/profile/twitter_authorize' : null;
				$this->User->twitter_callback( $url_redirect );
				exit;
			}

			if ( !$this->session( 'access_token' ) OR $action == 'connect' ) {
			    $this->User->twitter_connect();
			    exit;
			}

			$this->User->twitter();
			$this->redirect( '/' );
		}

		public function email_verify( $email )
		{
			$result = $this->User->email_verify($email);

			if (!$result)
				echo json_encode( array( 'status' => true ) );
			else
				echo json_encode( array( 'status' => false ) );
		}

		public function register()
		{
			$return = $this->User->add();

			if ( !$return->status ) {
				$return = $return->data;
				$return['status'] = false;
				
				$this->ajax($return);
			}
			else {
				$email = base64_encode($this->User->post_email);

				$body_mail = $this->template_get_content( 'confirmation', '_Mail' );
				$body_mail = str_replace( '#USER_NAME', $this->User->post_name, $body_mail );
				$body_mail = str_replace( '#USER_EMAIL', $email, $body_mail );
				$body_mail = str_replace( '#SITE_URL', $_SERVER['HTTP_HOST'], $body_mail );

				$send_mail = $this->mail( ACTIVATION_MAIL, $body_mail, utf8_decode( 'Bem-vindo novo usuário' ) );
				$send_mail->AddAddress( base64_decode( $email ), $this->User->post_name );
				$send_mail->send();

				$this->ajax($return);
			}
		}

		public function confirmation( $email = null )
		{
			if ( !$email )
				$this->redirect( '/' );

			$result = $this->User->activation( $email );

			$this->message( 'register_confirmation', $result );

			$this->redirect( '/' );
		}

		public function forgot_password()
		{
			$email = isset( $_POST['email'] ) ? $_POST['email'] : null;

			if ( $email ) {
				$user = $this->User->email_verify( $email, 'email, name' );
				
				if ( !$user ) {
					echo json_encode( array( 'status' => false ) );
					exit;
				}

				$this->User->change_status( $email, 2 ); // Change password mode
				$user->email = base64_encode( $user->email );

				$body_mail = $this->template_get_content( 'forgot_password', '_Mail' );
				$body_mail = str_replace( '#USER_NAME', $user->name, $body_mail );
				$body_mail = str_replace( '#USER_EMAIL', $user->email, $body_mail );
				$body_mail = str_replace( '#SITE_URL', $_SERVER['HTTP_HOST'], $body_mail );

				$send_mail = $this->mail( ACTIVATION_MAIL, $body_mail, utf8_decode( 'Redefinição de senha' ) );
				$send_mail->AddAddress( base64_decode( $user->email ), $user->name );
				$send_mail->send();

				echo json_encode( array( 'status' => true ) );
			}
			else
				echo json_encode( array( 'status' => false ) );
		}
	}
?>