<?php
    class SearchController extends AppController
	{
		public function index()
		{
			$title = $this->get_param('titulo');
			$cast = $this->get_param('elenco');
			$director = $this->get_param('direcao');
			$gender = $this->get_param('genero');
			$type = $this->get_param('tipo');
			//$genders = $this->Gender->all();

			$result = $this->Movie->search( $title, $cast, $director, $gender, $type );

			//$this->set( 'genders', $genders );
			$this->set( 'title', $title );
			$this->set( 'cast', $cast );
			$this->set( 'director', $director );
			$this->set( 'gender', $gender );
			$this->set( 'result', $result );
			$this->set( 'type', $type );
			$this->show( 'index' );
		}
	}
?>