<?php
    class FeedbackController extends AppController
	{
		public function index()
		{
			$result = $this->Feedback->save();

			if ( $result !== false ) {
				$admins = $this->User->get_admins();
				$body_mail = $this->template_get_content( 'template', '_Mail' );

				$body_mail = str_replace( '#USER_NAME', base64_decode( $this->session( 'username' ) ), $body_mail );
				$body_mail = str_replace( '#SITE_URL', $_SERVER['HTTP_HOST'], $body_mail );
				
				$send_mail = $this->mail( ACTIVATION_MAIL, $body_mail, utf8_decode( 'Feedback do usuário' ) );

				foreach ( $admins as $admin ) {
					$send_mail->AddAddress( $admin->email );
				}

				$send_mail->send();
			}

			$this->ajax( array( 'status' => $result ) );
		}
	}
?>