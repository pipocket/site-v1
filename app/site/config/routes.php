<?php
    //Actions
    Route::set('action', 'email-verify', 'email_verify');
	Route::set('action', 'informacoes', 'informations');
	Route::set('action', 'sair', 'out');
	Route::set('action', 'confirmacao', 'confirmation');
	Route::set('action', 'forgot-password', 'forgot_password');
	Route::set('action', 'redefine-password', 'redefine_password');
	Route::set('action', 'new-password', 'new_password');
	Route::set('action', 'next-movie', 'next_movie');
	Route::set('action', 'movie-ranking', 'movie_ranking');
	Route::set('action', 'movie-next', 'movie_next');

	//Controllers
	Route::set('controller', 'faca-parte', 'signup');
	Route::set('controller', 'busca', 'search');
	Route::set('controller', 'filme', 'movie');
	Route::set('controller', 'perfil', 'profile');
	Route::set('controller', 'termos', 'terms');
	Route::set('controller', 'privacidade', 'privacy');
	Route::set('controller', 'sobre', 'about');
?>