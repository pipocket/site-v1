<?php
	// Call global config
	include AURA_ROOT . DS . 'vendors' . DS . 'global_config.php';
	$global_config = new global_config( 'set_api_keys' );
	$api_keys = $global_config->set_api_keys();
	$media_files = $global_config->set_upload( true );

	// Defines default controller in app
	define('AURA_DEFAULT_CONTROLLER', 'home');
	// Defines default action in app
	define('AURA_DEFAULT_ACTION', 'index');
	
	// Defines SMTP configuration
	define('AURA_SMTP_HOST', 'smtp.gmail.com');
	define('AURA_SMTP_AUTH', true); 	 // (bolean) smtp authentication
	define('AURA_SMTP_LANG', 'br'); 
	define('AURA_SMTP_USERNAME', 'noreply@pipocket.com'); // email
	define('AURA_SMTP_PASSWORD', 'noreply#pppckt');
	define('AURA_SMTP_PORT', '465');
	define('AURA_SMTP_FROMNAME', 'Pipocket'); // title message
	
	// Defines folder for master pages
	define('AURA_SMARTY_MASTERPAGES_FOLDER', '_MasterPages');
	define('AURA_SMARTY_MASTERPAGE_DEFAULT', 'master');
	
	// Token to be added to md5
	define('AURA_MD5_TOKEN', '@#$%^67dfs&*()[');

	// Project
	define('PROJECT_NAME', 'Pipocket');

	// Activation mail
	define('ACTIVATION_MAIL', 'noreply@pipocket.com');

	// Social
	foreach( $api_keys as $key => $value )
		define($key, $value);

	// Media files
	foreach( $media_files as $key => $value )
		define($key, $value);
?>